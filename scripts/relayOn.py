import socket
import argparse
from dotenv import dotenv_values

config = dotenv_values("/home/pointer/remote/environment")

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

parser = argparse.ArgumentParser()
parser.add_argument('v', type=int, default=1, 
                    help='an integer for the accumulator')

args = parser.parse_args()
v = args.v

msg = b''
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')
msg += b'%b' % (v).to_bytes(1,'big')

s.sendto(msg, (config["LOCAL_TRANSPORTER"], int(config["RELAY_PORT"]))) 
