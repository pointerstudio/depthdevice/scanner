import sys
import os
import serial
import time
import glob

import socket
import json

from dotenv import dotenv_values

config = dotenv_values("/home/pointer/remote/environment")

ser = serial.Serial(
    port=glob.glob('/dev/ttyUSB*')[0],
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0.01
)

if ser.isOpen():
    ser.close()
time.sleep(0.1)
ser.open()
time.sleep(0.1)

# syncing, because controller does not know how to properly handle messages
synced=False
while not synced:
    time.sleep(0.01)
    ser.flushInput()
    ser.write(b'%i\r' % 127)
    time.sleep(0.1)
    r = ser.read(256)
    if r[-5:] == b'\r\nCH:':
        synced=True
        print("synced")
        ser.write(b'%i\r' % 0)
        time.sleep(0.01)
        ser.write(b'%i\r' % 127)
    else:
        print(f"not synced {r}")

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((config["LOCAL_TRANSPORTER"],int(config["LIGHT_PORT"])))

runLoop=True

START_MARKER=128

fade = ['N','B','U','a','1','|','^','`',' ']

for j in range (0,127):
    v = 127 - j
    data = [v,v,v,v,v,v]
    # debug = ""
    # clrs = ""

    # ser.write(b'%i\r' % 0)
    # time.sleep(0.001)
    # ser.write(b'%i\r' % v)
    # time.sleep(0.001)

    for i in range(0, 6):
        c=i+1
        ser.write(b'%i\r' % c)
        time.sleep(0.001)
        ser.write(b'%i\r' % data[i])
        time.sleep(0.001)
        # clrIndex = int(data[i] * (8.0/127.0))
        # clr = fade[clrIndex]
        # clrs += f"{clr}"
        # debug += f"{data[i]} "
    # print(f"{clrs}  {debug}")

    print(data)
    time.sleep(0.1)

exit(0)

