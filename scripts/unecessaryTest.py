import glob
import serial
import time
import usb

ser = serial.Serial(
    port=glob.glob('/dev/ttyUSB2')[0],
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0.1
)

def write(s,msg,read=False):
    s.write(f"{msg}\r".encode())
    output = ""
    try:
        incoming = s.read(256)
        print("success")
        output =  incoming
    except:
        print("error")
        output = "error while waiting"
    return output
                    

if not ser.isOpen():
    ser.open()
time.sleep(0.1)

# syncing, controller might wait for either CH or VAL
synced=False
while not synced:
    time.sleep(0.01)
    r = write(ser, 0, True)
    print(r)
    if r[-3:] == b"CH:":
        synced=True
        print("synced")
    else:
        print(f"not synced {r}")


for i in range(0,8):
    time.sleep(0.1)

    c = i + 1

    write(ser,c)
    write(ser,1)
    
    time.sleep(0.1)
    
    write(ser,c)
    write(ser,0)
#
#exit(1)
#
#print("on")
#time.sleep(0.01)
#
#ser.write(b'%i' % 0)
#time.sleep(0.01)
#
#ser.write(b'\r\n')
#time.sleep(0.01)
#
#ser.write(b'%i' % 0)
#time.sleep(0.01)
#
#ser.write(b'\r\n')
#print("off")
#time.sleep(1)
#
#print("range on")
#for i in range(0,8):
#    c = i + 1
#
#    ser.write(b'%i' % c)
#    time.sleep(0.01)
#    r = ser.read(256)
#    print(f"channel {c} {r}")
#    
#    ser.write(b'\r\n')
#    time.sleep(0.01)
#    r = ser.read(256)
#    print(f"            {r}")
#    
#    ser.write(b'%i' % 1)
#    time.sleep(0.01)
#    
#    ser.write(b'\r\n')
#
#    time.sleep(0.01)
#    r = ser.read(256)
#    print(r)
#    time.sleep(1)
#
#print("range off")
#for i in range(0,8):
#    c = i + 1
#    
#    ser.write(b'%i' % c)
#    time.sleep(0.01)
#    
#    ser.write(b'\r\n')
#    time.sleep(0.01)
#    
#    ser.write(b'%i' % 0)
#    time.sleep(0.01)
#    
#    ser.write(b'\r\n')
#    time.sleep(1)
