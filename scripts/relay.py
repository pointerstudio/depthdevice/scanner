import sys
import os
import serial
import time
import glob

import socket
import json

from dotenv import dotenv_values

config = dotenv_values("/home/pointer/remote/environment")

ser = serial.Serial(
    port=glob.glob('/dev/ttyUSB1')[0],
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0.01
)

if ser.isOpen():
    ser.close()
time.sleep(0.1)
ser.open()
time.sleep(0.1)

# syncing, because controller could be waiting for either channel or value
synced=False
while not synced:
    time.sleep(0.01)
    ser.flushInput()
    ser.write(b'%i\r' % 0)
    time.sleep(0.1)
    r = ser.read(256)
    if r[-3:] == b'CH:':
        synced=True
        print("synced")
    else:
        print(f"not synced {r}")

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((config["LOCAL_TRANSPORTER"],int(config["RELAY_PORT"])))

runLoop=True

while runLoop:
    data, address = s.recvfrom(8)
    print(data)
    debug = ""
    clrs = ""
    iv = int(1)
    bv = iv.to_bytes(1,'big')
    # ser.write(b'%b\r\n' % bv)
    ser.write(b'\x01')
    ser.write(b'\r\n')
    time.sleep(0.01)
    ser.write(b'\x01')
    ser.write(b'\r\n')
    time.sleep(0.01)
    time.sleep(0.01)
    r = ser.read(256)
    print(r)
    for i in range(0, len(data)):
        c=i+1
        debug += f"{data[0]} "
    print(f"{debug}")

exit(0)

