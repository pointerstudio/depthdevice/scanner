import socket
import time
from dotenv import dotenv_values

config = dotenv_values("/home/pointer/remote/environment")

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

for i in range(0,127): 
    v = 127 - i
    msg = b''
    msg += b'%b' % (v).to_bytes(1,'big')
    msg += b'%b' % (v).to_bytes(1,'big')
    msg += b'%b' % (v).to_bytes(1,'big')
    msg += b'%b' % (v).to_bytes(1,'big')
    msg += b'%b' % (v).to_bytes(1,'big')
    msg += b'%b' % (v).to_bytes(1,'big')
    
    s.sendto(msg, (config["LOCAL_TRANSPORTER"], int(config["LIGHT_PORT"]))) 
    time.sleep(0.01)
    print("sent")
