import socket
from dotenv import dotenv_values

config = dotenv_values("/home/pointer/remote/environment")

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

msg = b''
msg += b'%b' % (127).to_bytes(1,'big')
msg += b'%b' % (127).to_bytes(1,'big')
msg += b'%b' % (127).to_bytes(1,'big')
msg += b'%b' % (127).to_bytes(1,'big')
msg += b'%b' % (127).to_bytes(1,'big')
msg += b'%b' % (127).to_bytes(1,'big')

s.sendto(msg, (config["LOCAL_TRANSPORTER"], int(config["LIGHT_PORT"]))) 
