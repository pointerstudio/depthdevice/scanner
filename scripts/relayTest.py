import sys
import os
import serial
import time
import glob

ser = serial.Serial(
    port=glob.glob('/dev/ttyUSB0')[0],
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=0.01
)

ser.close()
ser.open()

# controller could be either waiting for CH or VAL
# sync first

# all on
ser.write(b'\x00')
ser.write(b'\r')
time.sleep(0.01)
ser.write(b'\x01')
ser.write(b'\r')

time.sleep(10.0)

# all off
ser.write(b'\x00')
ser.write(b'\r')
time.sleep(0.01)
ser.write(f'127\r'.encode())
time.sleep(0.01)

exit(0)

