#!/bin/bash

# get compatible dependencies hash
CUR_PWD=$(pwd)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

for a in `cat addons.make`; do
    echo "compatible $a hash:"
    cd ${DIR}/../../../addons/"$a" && git rev-parse HEAD && cd $CUR_PWD
    echo ""
done

