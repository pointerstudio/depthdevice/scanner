#!/bin/bash

entryDirectory=$(pwd)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

if [ -n "$PG_OF_PATH" ]; then
    echo "detected oF-root $PG_OF_PATH"
else
    echo "did not find PG_OF_PATH"
    echo "what is your openframeworks path?"
    read -e -p "PG_OF_PATH:" PG_OF_PATH
fi

cd $PG_OF_PATH/addons
git clone --depth=1 git@gitlab.com:pointerstudio/utils/ofxProfiler

git clone --depth=1 git@gitlab.com:pointerstudio/utils/ofxCv

git clone --depth=1 git@gitlab.com:pointerstudio/apop/ofxPCL

git clone --depth=1 git@gitlab.com:pointerstudio/apop/ofxDepthDevice

git clone --depth=1 http://github.com/roymacdonald/ofxCameraSaveLoad

git clone --depth=1 http://github.com/bakercp/ofxJSONRPC

git clone --depth=1 http://github.com/bakercp/ofxHTTP

export OF_ROOT=$PG_OF_PATH && export ADDON_NAME=ofxHTTP

cd ofxHTTP/scripts/ci && ./install.sh

cd $DIR/bin/data
git clone --depth=1 git@gitlab.com:pointerstudio/apop/webcontroller

cd $entryDirectory

