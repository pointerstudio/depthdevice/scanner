#include "VisualParameters.h"

void VisualParameterGroup::setup(string name){
    group.setName(name);
    group.add(backgroundColor.set("backgroundColor", ofFloatColor::black));
    group.add(queueNumberColor.set("queueNumberColor", ofFloatColor::white));
    group.add(gridXColor.set("gridXColor", ofFloatColor::red));
    group.add(gridYColor.set("gridYColor", ofFloatColor::green));
    group.add(gridZColor.set("gridZColor", ofFloatColor::blue));
    group.add(drawGrid.set("drawGrid", true));
    group.add(littleGridNumbers.set("littleGridNumbers", true));
    group.add(littleGridNumberColor.set("littleGridNumberColor", ofFloatColor::white));
    group.add(rotationSpeed.set("rotationSpeed", glm::vec3(0.1), glm::vec3(-1), glm::vec3(1)));
    group.add(distance.set("distance", 5400, 0, 100000));
    group.add(smoothing.set("smoothing", 0.1, 0, 1));
    group.add(shaderVariable0.set("shaderVariable0", 0.0, 0, 1));
    group.add(shaderVariable1.set("shaderVariable1", 0.0, 0, 1));
    group.add(shaderVariable2.set("shaderVariable2", 0.0, 0, 1));
    group.add(shaderVariable3.set("shaderVariable3", 0.0, 0, 1));
    group.add(shaderVariable4.set("shaderVariable4", 0.0, 0, 1));
    group.add(shaderVariable5.set("shaderVariable5", 0.0, 0, 1));
    group.add(shaderVariable6.set("shaderVariable6", 0.0, 0, 1));
    group.add(shaderColor0.set("shaderColor0", ofFloatColor::white));
    group.add(shaderColor1.set("shaderColor1", ofFloatColor::white));

    ofLogNotice("VisualParameterGroup") <<  " (" << group.getName() << ")";
}
void VisualParameters::mix(VisualParameterGroup & other){
    mixIn(backgroundColor, other.backgroundColor.get(), other.smoothing.get());
    mixIn(queueNumberColor, other.queueNumberColor.get(), other.smoothing.get());
    mixIn(gridXColor, other.gridXColor.get(), other.smoothing.get());
    mixIn(gridYColor, other.gridYColor.get(), other.smoothing.get());
    mixIn(gridZColor, other.gridZColor.get(), other.smoothing.get());
    mixIn(rotationSpeed, other.rotationSpeed.get(), other.smoothing.get());
    mixIn(distance, other.distance.get(), other.smoothing.get());
    drawGrid = other.drawGrid;
    littleGridNumbers = other.littleGridNumbers;
    mixIn(littleGridNumberColor, other.littleGridNumberColor.get(), other.smoothing.get());

    mixIn(shaderVariable0, other.shaderVariable0.get(), other.smoothing.get());
    mixIn(shaderVariable1, other.shaderVariable1.get(), other.smoothing.get());
    mixIn(shaderVariable2, other.shaderVariable2.get(), other.smoothing.get());
    mixIn(shaderVariable3, other.shaderVariable3.get(), other.smoothing.get());
    mixIn(shaderVariable4, other.shaderVariable4.get(), other.smoothing.get());
    mixIn(shaderVariable5, other.shaderVariable5.get(), other.smoothing.get());
    mixIn(shaderVariable6, other.shaderVariable6.get(), other.smoothing.get());
    mixIn(shaderColor0, other.shaderColor0.get(), other.smoothing.get());
    mixIn(shaderColor1, other.shaderColor1.get(), other.smoothing.get());
}
void VisualParameters::setShader(ofShader & shader){
    shader.setUniform1f("shaderVariable0", shaderVariable0);
    shader.setUniform1f("shaderVariable1", shaderVariable1);
    shader.setUniform1f("shaderVariable2", shaderVariable2);
    shader.setUniform1f("shaderVariable3", shaderVariable3);
    shader.setUniform1f("shaderVariable4", shaderVariable4);
    shader.setUniform1f("shaderVariable5", shaderVariable5);
    shader.setUniform1f("shaderVariable6", shaderVariable6);
    shader.setUniform4f("shaderColor0", shaderColor0);
    shader.setUniform4f("shaderColor1", shaderColor1);
}
