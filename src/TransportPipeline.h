#pragma once

#include "ofMain.h"
#include "ofxProfiler.h"
//
//#define PCL_NO_PRECOMPILE // !! BEFORE ANY PCL INCLUDE!!
#include "ofxPCL.h"
#include "BulletPhysics.h"
//#include "PCLHelper.h"
//#include <pcl/pcl_macros.h>
//#include <pcl/point_types.h>
//#include <pcl/point_cloud.h>
//#include <pcl/io/pcd_io.h>
//#include <pcl/filters/statistical_outlier_removal.h>
//#include <pcl/filters/impl/statistical_outlier_removal.hpp>
//#include <pcl/filters/sampling_surface_normal.h>
//#include <pcl/features/normal_3d.h>
//#include <pcl/filters/voxel_grid.h>

class TransportPipeline {
    public:

        TransportPipeline();
        ~TransportPipeline();

        enum TransportStage {
            Empty = 0,
            Collecting,
            CalculatingNormals,
            RemoveOutlier,
            BalanceMass,
            CreateLODs,
            Waiting,
            Idle,
        };

        map <TransportStage, string> transportStageString = {
            {Collecting, "Collecting"},
            {CalculatingNormals, "CalculatingNormals"},
            {RemoveOutlier, "RemoveOutlier"},
            {BalanceMass, "BalanceMass"},
            {CreateLODs, "CreateLODs"},
            {Waiting, "Waiting"},
            {Idle, "Idle"},
        };

        struct CollectingRaw {
            ofVboMesh mesh;
            ofPixels texture;
            glm::mat4 extrinsics;
        };

        struct Interput {
            vector <ofMesh> meshes;
            vector <ofPixels> textures;
            vector <string> names;
            void clear(){
                meshes.clear();
                textures.clear();
                names.clear();
            }
        };

        struct LODput {
            vector <ofMesh> meshes;
            vector <string> names;
            void clear(){
                meshes.clear();
                names.clear();
            }
        };

        struct Output {
            vector <ofMesh> meshes;
            vector <ofPixels> textures;
            vector <string> names;
            void clear(){
                meshes.clear();
                textures.clear();
                names.clear();
            }
        };

        void setup(int _nDevices, string name);
        void update();
        void draw(const uint64_t & now, ofShader & mainShader);

        void setStage(TransportStage s);
        TransportStage getStage();
        string getStageString();

        bool input(const ofVboMesh & mesh,
                   const ofPixels & texture,
                   const string & name,
                   const glm::mat4 & extrinsics);

        void calculateNormals(const CollectingRaw raw);
        void keyReleased(int key);

        const Output getOutput();
        const Output getDrawput();
        const LODput getLODput();

        ofParameter <float> normal_searchRadius;
        ofParameter <bool> normal_recalculate;

        ofParameter <float> outlier_kMean;
        ofParameter <float> outlier_mulTresh;
        ofParameter <bool> outlier_recalculate;

        ofParameter <string> lods_levels_string;
        ofParameter <bool> lods_recalculate;

        ofParameter <float> pointSize;

        vector <int> lods_levels;

        int nDevices;
        string scanName;

        std::atomic <bool> prepared;
        std::atomic <bool> preparedLODs;
        std::atomic <bool> saved;
        std::atomic <bool> savedLODs;

        std::atomic <bool> recalculating;

        std::thread pipe;
        std::atomic <bool> isPipeRunning;

        std::atomic <bool> showMirror;
        //ofShader mainShader;

        void setRecalculation(bool recalculate, TransportStage stage);

    private:
        void preparePipe();
        atomic <TransportStage> state_atomic;

        map <string, CollectingRaw> rawInput;
        mutex rawInput_mtx;

        Interput calculatinNormals_inter;
        Interput removeOutliers_inter;
        Interput balanceMass_inter;
        Interput createLODs_inter;

        atomic <bool> normal_recalculate_atomic;
        atomic <bool> outlier_recalculate_atomic;
        atomic <bool> lods_recalculate_atomic;

        Output drawput;
        mutex drawput_mtx;

        Output output;
        mutex output_mtx;

        LODput lodput;
        mutex lodput_mtx;

        BulletPhysics physics;
        ofNode centroidShift;
};
