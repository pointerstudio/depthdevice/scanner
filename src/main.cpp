#include "definitions.h"
#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main(){
    string profilerJson = ofGetTimestampString() + "_result.json";
    OFX_PROFILER_BEGIN_SESSION("transporterPool", profilerJson);
    ofGLFWWindowSettings settings;
//    settings.numSamples = 4;
    settings.setSize(MAIN_WIDTH, MAIN_HEIGHT);
    settings.setPosition(glm::vec2(0, 0));
    //settings.setFSAASamples(4);
    settings.setGLVersion(4, 6);
    settings.windowMode = OF_WINDOW;
    settings.decorated = true;

    vector <shared_ptr <ofAppBaseWindow> > windows;

    shared_ptr <ofApp> mainApp(new ofApp);

    // main window
    shared_ptr <ofAppBaseWindow> mainWindow = ofCreateWindow(settings);
    mainWindow->setVerticalSync(false);
    windows.push_back(mainWindow);

    ofRunApp(windows[0], mainApp);
    ofRunMainLoop();

    OFX_PROFILER_END_SESSION();
}
