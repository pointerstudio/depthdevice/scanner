#include "TransportPipeline.h"
#include "Utility.h"
#include "ofxPCL.h"
//#include "PCLHelper.h"
#include <chrono>
#include <iomanip>
#include <thread>

TransportPipeline::TransportPipeline(){
    showMirror.store(true);
    setStage(Empty);
    preparePipe();
    physics.setup();
}

TransportPipeline::~TransportPipeline(){
    isPipeRunning.store(false);
    pipe.join();
    physics.exit();
}

void TransportPipeline::setup(int _nDevices, string name){
    scanName = name;
    showMirror.store(true);
    prepared.store(false);
    saved.store(false);
    preparedLODs.store(false);
    savedLODs.store(false);
    nDevices = _nDevices;

    setStage(Collecting);

    unique_lock <mutex> rawInput_lock(rawInput_mtx);
    rawInput.clear();
    drawput.clear();
    lodput.clear();
    physics.clear();

    //mainShader.load("shader/normals.vert", "shader/normals.frag");

    normal_recalculate_atomic.store(normal_recalculate.get());
    outlier_recalculate_atomic.store(outlier_recalculate.get());
    lods_recalculate_atomic.store(lods_recalculate.get());
    recalculating.store(
        normal_recalculate_atomic.load()
        && outlier_recalculate_atomic.load()
        && lods_recalculate_atomic.load());
}

void TransportPipeline::setRecalculation(bool recalculate, TransportStage stage){
    switch(stage){
     case CalculatingNormals: {
         normal_recalculate_atomic.store(recalculate);
         break;
     }

     case RemoveOutlier: {
         outlier_recalculate_atomic.store(recalculate);
         break;
     }

     case CreateLODs: {
         lods_recalculate_atomic.store(recalculate);
         break;
     }

     default:
         break;
    }
    recalculating.store(
        normal_recalculate_atomic.load()
        && outlier_recalculate_atomic.load()
        && lods_recalculate_atomic.load());
}

void TransportPipeline::update(){
    physics.update();
}

void TransportPipeline::draw(const uint64_t & millis,
                             ofShader & mainShader){
    OFX_PROFILER_FUNCTION();
    float time = millis * 0.001f;
    auto scan = getDrawput();
    physics.nodes[0].transformGL();
    centroidShift.transformGL();
    if(scan.meshes.size() > 0){
        auto stage = state_atomic.load();
        if(stage == CalculatingNormals){
            //mainShader.setUniform1f("normalMix", 1);
        }else if(stage == RemoveOutlier){
            //mainShader.setUniform1f("normalMix", sin(time) * 0.5 + 0.5);
        }else if(stage == BalanceMass){
            //mainShader.setUniform1f("normalMix", sin(time) * 0.5 + 0.5);
        }else if(stage == CreateLODs){
            //mainShader.setUniform1f("normalMix", sin(time) * 0.5 + 0.5);
        }else if(stage == Waiting){
            //mainShader.setUniform1f("normalMix", 0);
        }

        for(const auto & kp: scan.meshes){
            glPointSize(pointSize.get());
            kp.draw();
        }

    }else{
        unique_lock <mutex> rawInput_lock(rawInput_mtx);
        for(auto & kp: rawInput){
            glPointSize(pointSize.get());
            kp.second.mesh.drawVertices();
        }
    }
    physics.nodes[0].restoreTransformGL();
    centroidShift.restoreTransformGL();
}

// watch out counterintuitive,
// returns true when all input is handed in
bool TransportPipeline::input(const ofVboMesh & mesh,
                              const ofPixels & texture,
                              const string & name,
                              const glm::mat4 & extrinsics){

    rawInput[name] = CollectingRaw({mesh,
                                    texture,
                                    extrinsics});
    physics.addBody(mesh);
    bool out = rawInput.size() == nDevices;
    if(out){
        showMirror.store(false);
        ofNode node;
        centroidShift = node;
        physics.transformBody(0, node, false);
    }
    return out;
}

const TransportPipeline::Output TransportPipeline::getOutput(){
    lock_guard <mutex> output_lock(output_mtx);
    return output;
}
const TransportPipeline::Output TransportPipeline::getDrawput(){
    lock_guard <mutex> drawput_lock(drawput_mtx);
    return drawput;
}

const TransportPipeline::LODput TransportPipeline::getLODput(){
    lock_guard <mutex> lodput_lock(lodput_mtx);
    return lodput;
}

void TransportPipeline::preparePipe(){
    isPipeRunning.store(true);
    pipe = std::thread([&] {
        while(isPipeRunning.load()){
            switch(state_atomic.load()){
             case Collecting: {
                 unique_lock <mutex> rawInput_lock(rawInput_mtx);
                 if(rawInput.size() == nDevices){
                     setStage(CalculatingNormals);
                     //rawInput_lock.unlock();
                 }
                 break;
             }

             case CalculatingNormals: {
                 OFX_PROFILER_SCOPE("TransportPipeline::CalculatingNormals");
                 unique_lock <mutex> rawInput_lock(rawInput_mtx);
                 map <string, CollectingRaw> temporary = rawInput;
                 rawInput_lock.unlock();

                 for(auto & kp : temporary){
                     auto & extrinsics = kp.second.extrinsics;
                     auto & mesh = kp.second.mesh;
                     mesh.setMode(OF_PRIMITIVE_POINTS);

                     pcl::PointCloud <pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud <pcl::PointXYZ> );
                     for(auto & ov : mesh.getVertices()){
                         glm::vec3 v = glm::vec4(ov.x, ov.y, ov.z, 1);
                         pcl::PointXYZ p(v.x, v.y, v.z);
                         cloud->points.push_back(p);
                     }


                     // Create the normal estimation class, and pass the input dataset to it
                     pcl::NormalEstimation <pcl::PointXYZ, pcl::Normal> ne;
                     ne.setInputCloud(cloud);

                     glm::vec3 origin = extrinsics * glm::vec4(0, 0, 0, 1);
                     ne.setViewPoint(origin.x, origin.y, origin.z);

                     // Create an empty kdtree representation, and pass it to the normal estimation object.
                     // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
                     pcl::search::KdTree <pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree <pcl::PointXYZ> ());
                     ne.setSearchMethod(tree);

                     // Output datasets
                     pcl::PointCloud <pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud <pcl::Normal> );

                     // Use all neighbors in a sphere of radius 3cm
                     ne.setRadiusSearch(normal_searchRadius.get());

                     // Compute the features
                     ne.compute(*cloud_normals);

                     for(auto & p : cloud_normals->points){
                         mesh.addNormal(glm::vec3(p.normal_x,
                                                  p.normal_y,
                                                  p.normal_z));
                     }
                     // cloud_normals->size () should have the same size as the input cloud->size ()*

                 }

                 calculatinNormals_inter.clear();
                 for(auto & kp : temporary){
                     calculatinNormals_inter.meshes.push_back(kp.second.mesh);
                     calculatinNormals_inter.textures.push_back(kp.second.texture);
                     calculatinNormals_inter.names.push_back(kp.first);
                 }
                 unique_lock <mutex> drawput_lock(drawput_mtx);
                 drawput.meshes = calculatinNormals_inter.meshes;
                 drawput.textures = calculatinNormals_inter.textures;
                 drawput.names = calculatinNormals_inter.names;
                 drawput_lock.unlock();

                 unique_lock <mutex> output_lock(output_mtx);
                 output.meshes = calculatinNormals_inter.meshes;
                 output.textures = calculatinNormals_inter.textures;
                 output.names = calculatinNormals_inter.names;
                 output_lock.unlock();

                 if(!normal_recalculate_atomic.load()){
                     prepared.store(true);
                     setStage(RemoveOutlier);
                 }
                 ofLogNotice("TransportPipeline::pipe") << "fresh normals";
                 break;
             }

             case RemoveOutlier: {
                 OFX_PROFILER_SCOPE("TransportPipeline::RemoveOutlier");
                 removeOutliers_inter.clear();
                 for(const auto & m : calculatinNormals_inter.meshes){
                     pcl::PointCloud <pcl::PointXYZRGBNormal>::Ptr cloud(new pcl::PointCloud <pcl::PointXYZRGBNormal> );

                     int num_point = m.getVertices().size();

                     for(int i = 0; i < num_point; i++){
                         pcl::PointXYZRGBNormal p; // = cloud->points[i];
                         p.x = m.getVertices()[i].x;
                         p.y = m.getVertices()[i].y;
                         p.z = m.getVertices()[i].z;

                         p.normal_x = m.getNormals()[i].x;
                         p.normal_y = m.getNormals()[i].y;
                         p.normal_z = m.getNormals()[i].z;

                         p.r = ofFloatColor(m.getColors()[i]).r * 255.0;
                         p.g = ofFloatColor(m.getColors()[i]).g * 255.0;
                         p.b = ofFloatColor(m.getColors()[i]).b * 255.0;
                         cloud->points.push_back(p);

                         //p.tex_x = m.getTexCoords()[i].x;
                         //p.tex_y = m.getTexCoords()[i].y;
                     }
                     {
                         pcl::StatisticalOutlierRemoval <pcl::PointXYZRGBNormal> sor;
                         sor.setInputCloud(cloud);
                         sor.setMeanK(outlier_kMean.get());
                         sor.setStddevMulThresh(outlier_mulTresh.get());
                         sor.filter(*cloud);
                     }

                     {
                         pcl::StatisticalOutlierRemoval <pcl::PointXYZRGBNormal> sor;
                         sor.setInputCloud(cloud);
                         sor.setMeanK(outlier_kMean.get());
                         sor.setStddevMulThresh(outlier_mulTresh.get());
                         sor.filter(*cloud);
                     }

                     ofVboMesh mesh; //= ofxPCL::toOF(cloud);
                     mesh.disableIndices();
                     mesh.setMode(OF_PRIMITIVE_POINTS);
                     for(const auto & p : cloud->points){
                         glm::vec3 v = glm::vec3(p.x, p.y, p.z);
                         mesh.addVertex(v);
                         mesh.addNormal(glm::vec3(p.normal_x, p.normal_y, p.normal_z));
                         mesh.addColor(ofFloatColor(p.r / 255.0f, p.g / 255.0f, p.b / 255.0f));
                         //mesh.addTexCoord(glm::vec2(p.tex_x, p.tex_y));
                     }
                     removeOutliers_inter.meshes.push_back(mesh);
                 }
                 removeOutliers_inter.names = calculatinNormals_inter.names;
                 removeOutliers_inter.textures = calculatinNormals_inter.textures;

                 unique_lock <mutex> drawput_lock(drawput_mtx);
                 drawput.meshes = removeOutliers_inter.meshes;
                 drawput.textures = removeOutliers_inter.textures;
                 drawput.names = removeOutliers_inter.names;
                 drawput_lock.unlock();
                 if(!outlier_recalculate_atomic.load()){
                     setStage(BalanceMass);
                 }
                 ofLogNotice("TransportPipeline::pipe") << "fresh remove outliers";
                 break;
             }

             case BalanceMass: {
                 OFX_PROFILER_SCOPE("TransportPipeline::BalanceMass");
                 balanceMass_inter.clear();
                 glm::vec3 centroid;
                 for(const auto & m : removeOutliers_inter.meshes){
                     centroid = centroid + m.getCentroid();
                 }
                 centroid = centroid / double(removeOutliers_inter.meshes.size());

                 for(auto & m : removeOutliers_inter.meshes){
                     for(auto & vertex: m.getVertices()){
                         vertex -= centroid;
                     }
                     balanceMass_inter.meshes.push_back(m);
                 }
                 balanceMass_inter.names = removeOutliers_inter.names;
                 balanceMass_inter.textures = removeOutliers_inter.textures;

                 unique_lock <mutex> drawput_lock(drawput_mtx);
                 drawput.meshes = balanceMass_inter.meshes;
                 drawput.textures = balanceMass_inter.textures;
                 drawput.names = balanceMass_inter.names;
                 drawput_lock.unlock();

                 ofNode node;
                 node.setPosition(centroid);
                 physics.transformBody(0, node);
                 centroidShift = node;

                 setStage(CreateLODs);

                 ofLogNotice("TransportPipeline::pipe") << "fresh remove outliers";
                 break;
             }

             case CreateLODs: {
                 OFX_PROFILER_SCOPE("TransportPipeline::CreateLODS");
                 createLODs_inter.clear();

                 ofMesh allMeshes;
                 for(const auto & m : balanceMass_inter.meshes){
                     allMeshes.addVertices(m.getVertices());
                     allMeshes.addColors(m.getColors());
                     allMeshes.addNormals(m.getNormals());
                 }

                 pcl::PointCloud <pcl::PointXYZRGBNormal>::Ptr cloud(new pcl::PointCloud <pcl::PointXYZRGBNormal> );

                 int num_point = allMeshes.getVertices().size();

                 for(int i = 0; i < num_point; i++){
                     pcl::PointXYZRGBNormal p;     // = cloud->points[i];
                     p.x = allMeshes.getVertices()[i].x;
                     p.y = allMeshes.getVertices()[i].y;
                     p.z = allMeshes.getVertices()[i].z;

                     p.normal_x = allMeshes.getNormals()[i].x;
                     p.normal_y = allMeshes.getNormals()[i].y;
                     p.normal_z = allMeshes.getNormals()[i].z;

                     p.r = ofFloatColor(allMeshes.getColors()[i]).r * 255.0;
                     p.g = ofFloatColor(allMeshes.getColors()[i]).g * 255.0;
                     p.b = ofFloatColor(allMeshes.getColors()[i]).b * 255.0;
                     cloud->points.push_back(p);

                     //p.tex_x = allMeshes.getTexCoords()[i].x;
                     //p.tex_y = allMeshes.getTexCoords()[i].y;
                 }
                 for(const auto & lod : lods_levels){
                     cout << "lod level << " << lod << endl;
                     // the result cloud
                     pcl::PointCloud <pcl::PointXYZRGBNormal>::Ptr cloud_filtered(new pcl::PointCloud <pcl::PointXYZRGBNormal> );

                     // Create the filtering object
                     pcl::VoxelGrid <pcl::PointXYZRGBNormal> sor;
                     sor.setInputCloud(cloud);
                     //sor.setDownsampleAllData(true);
                     sor.setLeafSize(float(lod), float(lod), float(lod));
                     sor.filter(*cloud_filtered);
                     ofMesh mesh;
                     //mesh;// = ofxPCL::toOF(cloud_filtered);
                     for(const auto & p : cloud_filtered->points){
                         glm::vec3 v = glm::vec3(p.x, p.y, p.z);
                         mesh.addVertex(v);
                         mesh.addNormal(glm::vec3(p.normal_x, p.normal_y, p.normal_z));
                         mesh.addColor(ofFloatColor(p.r / 255.0f, p.g / 255.0f, p.b / 255.0f));
                         //mesh.addTexCoord(glm::vec2(p.tex_x, p.tex_y));
                     }
                     mesh.disableIndices();
                     mesh.setMode(OF_PRIMITIVE_POINTS);
                     createLODs_inter.meshes.push_back(mesh);
                     createLODs_inter.names.push_back(ofToString(lod));
                 }
                 unique_lock <mutex> drawput_lock(drawput_mtx);
                 drawput.clear();
                 //drawput.meshes = {createLODs_inter.meshes[lods_levels.size() - 1]};
                 //drawput.names = {createLODs_inter.names[lods_levels.size() - 1]};
                 drawput.meshes = {createLODs_inter.meshes[0]};
                 drawput.names = {createLODs_inter.names[0]};
                 drawput_lock.unlock();
                 unique_lock <mutex> lodput_lock(lodput_mtx);
                 lodput.clear();
                 lodput.meshes = createLODs_inter.meshes;
                 lodput.names = createLODs_inter.names;
                 lodput_lock.unlock();

                 if(!lods_recalculate_atomic.load()){
                     preparedLODs.store(true);
                     setStage(Waiting);
                 }
                 ofLogNotice("TransportPipeline::pipe") << "fresh create lods";
                 break;
             }

             case Waiting: {
                 OFX_PROFILER_SCOPE("TransportPipeline::Waiting");
                 if(normal_recalculate_atomic.load()){
                     setStage(CalculatingNormals);
                     ofLogNotice("TransportPipeline")
                     << "recalculate normals";
                 }else if(outlier_recalculate_atomic.load()){
                     setStage(RemoveOutlier);
                     ofLogNotice("TransportPipeline")
                     << "recalculate outliers";
                 }else if(lods_recalculate_atomic.load()){
                     setStage(CreateLODs);
                     ofLogNotice("TransportPipeline")
                     << "recalculate LODs";
                 }else if(saved.load() && savedLODs.load()){
                     //ofLogNotice("Transportpipeline") << "just got news that the model is saved now, let's wait a bit";
                     std::this_thread::sleep_for(chrono::milliseconds(20));
                     setStage(Idle);
                     ofLogNotice("TransportPipeline")
                     << "Done";
                 }
                 break;
             }

             case Idle: {
                 OFX_PROFILER_SCOPE("TransportPipeline::Idle");
                 std::this_thread::sleep_for(chrono::milliseconds(20));
                 break;
             }

             default: {
                 OFX_PROFILER_SCOPE("TransportPipeline::default");
                 std::this_thread::sleep_for(chrono::milliseconds(20));
             }
            }
            std::this_thread::sleep_for(chrono::milliseconds(20));
        }
    });
}

void TransportPipeline::keyReleased(int key){
}

void TransportPipeline::setStage(TransportStage s){
    ofLogNotice("TransportPipeline::setStage") << s << endl;
    state_atomic.store(s);
}
TransportPipeline::TransportStage TransportPipeline::getStage(){
    return state_atomic.load();
}
string TransportPipeline::getStageString(){
    return transportStageString[getStage()];
}
