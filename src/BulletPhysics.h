#pragma once

#include "ofMain.h"

#include "ofxGui.h"

#include "btBulletDynamicsCommon.h"
#include "LinearMath/btVector3.h"
#include "LinearMath/btAlignedObjectArray.h"

struct PhysicsParameters {
    ofParameter <float> initRadius = 1.0f;
    ofParameter <float> initPerpendicular = 10000.0f * 0.05;
    ofParameter <glm::vec3> initPerpendicularVector = glm::vec3(0, 1, 1);
    ofParameter <float> initInertia = 100.0f;
    ofParameter <glm::vec3> initTorqueImpulse = glm::vec3(1000.0f, 1000.0f, 1000.0f) * 0.05;
    ofParameter <float> centerRadius = 10.0f;
    ofParameter <float> gravityForce = 50000.0f * 0.05;
    ofParameter <float> simSpeed = 1.0f / 60.0f;
    ofParameter <float> simSteps = 10;
    ofParameter <glm::vec3> gravityCenter = glm::vec3(0, 0, 0);
};
class BulletPhysics {
    public:
        BulletPhysics() = default;
        ~BulletPhysics() = default;
        void setup();
        void update();
        void draw();
        void exit();
        void clear();
        void addBody(const ofVboMesh & m);
        void transformBody(int index, const ofNode & node, bool addTransform = true);

        btScalar fToBtScalar(float f){
            return f;
        }
        btVector3 v3ToBtV3(glm::vec3 v){
            return btVector3(v.x, v.y, v.z);
        }
        vector <ofNode> nodes;
    protected:
        void createRigidBody(btScalar mass, btTransform transform, btCollisionShape * shape);

        btAlignedObjectArray <btCollisionShape *> collisionShapes;
        btBroadphaseInterface * overlappingPairCache;
        btDispatcher * dispatcher;
        btConstraintSolver * solver;
        btDefaultCollisionConfiguration * collisionConfig;
        btDiscreteDynamicsWorld * dynamicsWorld;
        mutex dynamicsWorld_mtx;

        btBoxShape * colShape;

        PhysicsParameters parameters;
};

