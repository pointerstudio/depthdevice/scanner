#pragma once

#include "definitions.h"
#include "ofMain.h"
#include "ofxDepthDevice.h"
#include "ofxGui.h"
#include "ofxAssimpModelLoader.h"
//#include "SerialRelay.h"
#define nil Boost_nil
#define Nil Boost_Nil
#include "ofxPCL.h"
#undef Nil
#undef nil
#include "ofxCameraSaveLoad.h"
#include "TransportPipeline.h"
#include "VisualParameters.h"

template <class T>
bool existsInVector(const vector <T> & v, T e){
    return find(v.begin(), v.end(), e) != v.end();
}

template <class T>
int positionInVector(const vector <T> & v, T e){
    auto it = find(v.begin(), v.end(), e);
    return it != v.end() ? std::distance(v.begin(), it) : -1;
}

struct AppParameters {
    ofParameter <bool> updateDevices;
    ofParameter <bool> drawDevices;
    ofParameter <bool> drawDebug;
    ofParameter <bool> cropping;
    ofParameter <bool> saveModel;
    ofParameter <bool> saveCalibration;
    ofParameter <bool> saveGlobalCalibration;
    ofParameter <void> quit;
    ofParameter <glm::vec3> center;
};

class DeviceParameters {
    public:
        ofParameter <string> serial;
        ofParameter <bool> draw_device;
        ofParameter <bool> draw_debug;
        ofParameter <bool> calibrationMom;
        ofParameter <bool> calibrationChild;
        ofParameter <bool> save_settings;
        ofParameter <bool> load_settings;
        ofParameter <int> position_index;
        ofParameter <glm::vec3> center;
        ofParameter <glm::vec3> centerFine;
        ofParameter <glm::vec3> position;
        ofParameter <glm::vec3> positionFine;
        ofParameter <glm::vec3> orientation;
        ofParameter <glm::vec3> orientationFine;
        ofParameter <glm::vec3> cropPosition;
        ofParameter <float> cropHeight;
        ofParameter <float> cropRadius;
        void setDeviceExtrinsics(shared_ptr <ofxDepthDevice::Kinect> & device, const AppParameters & appParameters);
        void setDeviceCropping(shared_ptr <ofxDepthDevice::Kinect> & device);
};

struct RefeynmanParameters {
    ofParameter <bool>      reset;
    ofParameter <bool>      save;
    ofParameter <bool>      load;
    ofParameter <bool>      downsample;
    ofParameter <ofVec3f>   leafSize;
    ofParameter <int>       iterations;
    ofParameter <int>       maxIterations;
    ofParameter <float>     correspondence;
    ofParameter <float>     incrementalCorrespondenceReduction;
    ofParameter <float>     normalSearchRadius;
    ofParameter <float>     transformationEpsilon;
    ofParameter <float>     transformRotationEpsilon;
};

class Refeynman {
    public:
        static void pairAlign(const ofVboMesh & mom,
                              const ofVboMesh & child,
                              const RefeynmanParameters & parameters,
                              glm::mat4 & theMatrix);
        static bool saveToDisk(const string & fileName, const glm::mat4 & cameraPoseMatrix);
        static bool loadFromDisk(const string & fileName, glm::mat4 & cameraPoseMatrix);
};

struct Screen {
    ofFbo fbo;
    ofEasyCam cam;
};

class ofApp : public ofBaseApp {

    enum AppAction {
        APP_ACTION_IDLE = 0,
        APP_ACTION_GENERATE_MARKER,
        APP_ACTION_INTRINSIC_CALIBRATION_COLLECT,
        APP_ACTION_INTRINSIC_CALIBRATION_CALIBRATE,
        APP_ACTION_EXTRINSIC_CALIBRATION_POSE,
        APP_ACTION_EXTRINSIC_CALIBRATION_REFEYNMAN,
        APP_ACTION_SAVE_MODEL,
        APP_ACTION_QUIT,
        APP_ACTION_NUM
    };

    vector<string> appActionStrings = {
        "APP_ACTION_IDLE",
        "APP_ACTION_GENERATE_MARKER",
        "APP_ACTION_INTRINSIC_CALIBRATION_COLLECT",
        "APP_ACTION_INTRINSIC_CALIBRATION_CALIBRATE",
        "APP_ACTION_EXTRINSIC_CALIBRATION_POSE",
        "APP_ACTION_EXTRINSIC_CALIBRATION_REFEYNMAN",
        "APP_ACTION_SAVE_MODEL",
        "APP_ACTION_QUIT",
        "APP_ACTION_NUM"
    };

    enum CameraAction {
        CAMERA_ACTION_DEFAULT = 0,
        CAMERA_ACTION_PERSPECTIVE,
        CAMERA_ACTION_ORTHO,
        CAMERA_ACTION_ALIGN_X,
        CAMERA_ACTION_ALIGN_Y,
        CAMERA_ACTION_ALIGN_Z,
        CAMERA_ACTION_ALIGN_FLIP,
        CAMERA_ACTION_NUM
    };

    vector<string> cameraActionStrings = {
        "DEFAULT",
        "PERSPECTIVE",
        "ORTHO",
        "ALIGN_X",
        "ALIGN_Y",
        "ALIGN_Z",
        "ALIGN_FLIP"
    };

    public:
        void setup();
        void update();
        void draw();
        void drawDebug(int deviceIndex);
        void exit();

        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        void changedPosition(glm::vec3 & e);
        void deviceParametersListener(ofAbstractParameter & e);
        void appParametersListener(ofAbstractParameter & e);
        void refeynmanParametersListener(ofAbstractParameter & e);
        void transportPipelineParametersListener(ofAbstractParameter & e);

        void appActionListener(int & e);
        void appActionGroupListener(ofAbstractParameter & e);

        void doCameraAction(const CameraAction & cameraAction);
        void cameraActionGroupListener(ofAbstractParameter & e);
        ofParameterGroup cameraActionGroup;

        AppParameters appParameters;
        ofParameter <int> appAction;
        ofParameterGroup appActionGroup;
        ofxGuiGroup appGlobalCalibrationGuiGroup;

        vector <shared_ptr <ofxDepthDevice::Kinect> > kirnects;
        vector <CalibrationHelper::Helper> calibrationHelpers;
        ofxPanel gui;
        vector <DeviceParameters> deviceParameters;
        vector <shared_ptr <ofxGuiGroup> > manualAdjustments;

        ofxGuiGroup refeynmanGuiGroup;
        RefeynmanParameters refeynmanParameters;

        ofxGuiGroup transportPipelineGuiGroup;

        ofxDepthDevice::Saviour saviour;
        ofxDepthDevice::Saviour saviourLODs;

        //SerialRelay serialRelay;
        ofxAssimpModelLoader kinect_azure_model;

        int reconstructionMethod = 0;

        shared_ptr <Screen> mainScreen;
        vector <shared_ptr <Screen> > screens;

        TransportPipeline transportPipeline;
        ofShader mainShader;
        bool showMirror = true;
        map <string, VisualParameterGroup> visualParameterGroups;
        VisualParameters visualParameters;

        void drawOurGridPlane(float stepSize, size_t numberOfSteps, VisualParameters & p){
            ofPushStyle();
            float scale = stepSize * numberOfSteps;
            float lineWidth = ofGetStyle().lineWidth;

            bool labels = p.littleGridNumbers;

            for(int iDimension = 0; iDimension < 2; iDimension++){
                for(size_t i = 0; i <= numberOfSteps; i++){
                    float yz = i * stepSize;

                    if(i == numberOfSteps || i == 0){
                        ofSetLineWidth(2); // central axis or cap line
                    }else if(i % 2 == 0){
                        ofSetLineWidth(1.5); // major
                    }else{
                        ofSetLineWidth(1); // minor
                    }

                    if(iDimension == 0){
                        ofDrawLine(0, yz, -scale, 0, yz, scale);
                        if(yz != 0){
                            ofDrawLine(0, -yz, -scale, 0, -yz, scale);
                        }
                    }else{
                        ofDrawLine(0, -scale, yz, 0, scale, yz);
                        if(yz != 0){
                            ofDrawLine(0, -scale, -yz, 0, scale, -yz);
                        }
                    }
                }
            }
            ofSetLineWidth(lineWidth);

            if(labels){
                ofSetColor(p.littleGridNumberColor);

                ofDrawBitmapString(ofToString(0), 0, 0, 0);

                for(float i = 1; i <= numberOfSteps; i++){
                    float yz = i * stepSize;
                    ofDrawBitmapString(ofToString(yz), 0, yz, 0);
                    ofDrawBitmapString(ofToString(-yz), 0, -yz, 0);
                    ofDrawBitmapString(ofToString(yz), 0, 0, yz);
                    ofDrawBitmapString(ofToString(-yz), 0, 0, -yz);
                }
            }
            ofPopStyle();
        }

        void drawOurGrid(float stepSize, size_t numberOfSteps, VisualParameters & p){
            if(p.drawGrid){
                ofPushStyle();
                bool labels = p.littleGridNumbers;

                {   // draw x plane
                    ofSetColor(p.gridXColor);
                    drawOurGridPlane(stepSize, numberOfSteps, p);
                }

                {   // draw y plane
                    ofSetColor(p.gridYColor);
                    glm::mat4 m = glm::rotate(glm::mat4(1.0), glm::half_pi <float>(), glm::vec3(0, 0, -1));
                    ofPushMatrix();
                    ofMultMatrix(m);
                    drawOurGridPlane(stepSize, numberOfSteps, p);
                    ofPopMatrix();
                }

                {   // draw z plane
                    ofSetColor(p.gridZColor);
                    glm::mat4 m = glm::rotate(glm::mat4(1.0), glm::half_pi <float>(), glm::vec3(0, 1, 0));
                    ofPushMatrix();
                    ofMultMatrix(m);
                    drawOurGridPlane(stepSize, numberOfSteps, p);
                    ofPopMatrix();
                }

                if(labels){
                    ofSetColor(p.littleGridNumberColor);
                    float labelPos = stepSize * (numberOfSteps + 0.5);
                    ofDrawBitmapString("x", labelPos, 0, 0);
                    ofDrawBitmapString("y", 0, labelPos, 0);
                    ofDrawBitmapString("z", 0, 0, labelPos);
                }
                ofPopStyle();
            }
        }
};
