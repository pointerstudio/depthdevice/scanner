#pragma once

#include "ofMain.h"

// helpers
template <class T>
void mixIn(T & a, const T & b, const float & s){
    a = a * s + (1 - s) * b;
}

class VisualParameterGroup {
    public:
        void setup(string name);

        ofParameter <ofFloatColor> backgroundColor;
        ofParameter <ofFloatColor> queueNumberColor;
        ofParameter <ofFloatColor> gridXColor;
        ofParameter <ofFloatColor> gridYColor;
        ofParameter <ofFloatColor> gridZColor;
        ofParameter <bool> drawGrid;
        ofParameter <bool> littleGridNumbers;
        ofParameter <ofFloatColor> littleGridNumberColor;
        ofParameter <glm::vec3> rotationSpeed;
        ofParameter <float> distance;
        ofParameter <float> smoothing;
        ofParameter <float> shaderVariable0;
        ofParameter <float> shaderVariable1;
        ofParameter <float> shaderVariable2;
        ofParameter <float> shaderVariable3;
        ofParameter <float> shaderVariable4;
        ofParameter <float> shaderVariable5;
        ofParameter <float> shaderVariable6;
        ofParameter <ofFloatColor> shaderColor0;
        ofParameter <ofFloatColor> shaderColor1;

        ofParameterGroup group;
};

class VisualParameters {
    public:
        void mix(VisualParameterGroup & other);
        void setShader(ofShader & shader);

        ofFloatColor backgroundColor = ofFloatColor::black;
        ofFloatColor queueNumberColor = ofFloatColor::black;
        ofFloatColor gridXColor = ofFloatColor::black;
        ofFloatColor gridYColor = ofFloatColor::black;
        ofFloatColor gridZColor = ofFloatColor::black;
        bool drawGrid = true;
        bool littleGridNumbers = true;
        ofFloatColor littleGridNumberColor = ofFloatColor::white;
        glm::vec3 rotationSpeed = glm::vec3(0.0);
        float distance = 5400;
        long double rotationTimes[3] = {0, 0, 0};

        float shaderVariable0;
        float shaderVariable1;
        float shaderVariable2;
        float shaderVariable3;
        float shaderVariable4;
        float shaderVariable5;
        float shaderVariable6;
        ofFloatColor shaderColor0;
        ofFloatColor shaderColor1;
};
