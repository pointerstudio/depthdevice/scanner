﻿#include "ofApp.h"

#include <chrono>
#include <glm/gtx/matrix_decompose.hpp>
#include <thread>
//--------------------------------------------------------------
void ofApp::setup(){

    //saviour.saveDirectory = ofGetEnv("LOCAL_TRANSPORTER_ORIGINALS");
    //saviourLODs.saveDirectory = ofGetEnv("LOCAL_TRANSPORTER_MODELS");

    uint64_t millis = ofGetSystemTimeMillis();
    ofSetFrameRate(60);

    gui.setup("gui", "settings.json", 20, 20);
    gui.setWidthElements(420);

    appActionGroup.setName("app_action_group");
    for(int i = 0; i < int(APP_ACTION_NUM); i++){
        string name = appActionStrings[i];
        //ofStringReplace(name, "APP_ACTION_", "");
        auto v = ofParameter <bool>(name, false);
        appActionGroup.add(v);
    }

    ofAddListener(appActionGroup.parameterChangedE(), this, &ofApp::appActionGroupListener);
    gui.add(appActionGroup);

    appAction.addListener(this, &ofApp::appActionListener);
    appAction.set(APP_ACTION_IDLE);

    //gui.add(appAction.set("app_action", 0, 0, int(APP_ACTION_NUM)));

    cameraActionGroup.setName("camera_action_group");
    for(int i = 0; i < int(CAMERA_ACTION_NUM); i++){
        string name = cameraActionStrings[i];
        auto v = ofParameter <void>(name);
        CameraAction ca = CameraAction(i);
        cameraActionGroup.add(v);
    }

    ofAddListener(cameraActionGroup.parameterChangedE(), this, &ofApp::cameraActionGroupListener);
    gui.add(cameraActionGroup);

    //gui.add(cameraAction.set("app_action", 0, 0, int(APP_ACTION_NUM)));

    ofParameterGroup appGroup;
    appGroup.setName("app_parameters");
    appGroup.add(appParameters.updateDevices.set("update_devices", true));
    appGroup.add(appParameters.drawDevices.set("draw_devices", true));
    appGroup.add(appParameters.drawDebug.set("draw_debug", true));
    appGroup.add(appParameters.cropping.set("cropping", true));
    appGroup.add(appParameters.saveModel.set("save_model", false));
    appGroup.add(appParameters.saveCalibration.set("save_all_calibration", false));
    appGroup.add(appParameters.quit.set("quit"));
//    appGroup.add(appParameters.orientation.set("orientation", glm::vec3(0), glm::vec3(-180), glm::vec3(180)));

    ofAddListener(appGroup.parameterChangedE(), this, &ofApp::appParametersListener);
    gui.add(appGroup);

    ofParameterGroup appGlobalSettingsGroup;
    appGlobalSettingsGroup.setName("global_calibration");
    appGlobalSettingsGroup.add(appParameters.saveGlobalCalibration.set("save_global_calibration", true));
    appGlobalSettingsGroup.add(appParameters.center.set("center", glm::vec3(0), glm::vec3(-5000), glm::vec3(5000)));

    appGlobalCalibrationGuiGroup.setup(appGlobalSettingsGroup, "calibration/global_calibration.json");
    ofAddListener(appGlobalSettingsGroup.parameterChangedE(), this, &ofApp::appParametersListener);
    gui.add(&appGlobalCalibrationGuiGroup);
    appGlobalCalibrationGuiGroup.minimizeAll();

    ofParameterGroup refeynmanGroup;
    refeynmanGroup.setName("refeynman_parameters");
    refeynmanGroup.add(refeynmanParameters.downsample.set("downsample", true));
    refeynmanGroup.add(refeynmanParameters.leafSize.set("leaf size", glm::vec3(6), glm::vec3(0.0001), glm::vec3(200)));
    refeynmanGroup.add(refeynmanParameters.iterations.set("iterations", 30, 1, 50));
    refeynmanGroup.add(refeynmanParameters.maxIterations.set("max iterations", 2, 1, 50));
    refeynmanGroup.add(refeynmanParameters.correspondence.set("max correspondence (mm)", 400, 0.01, 1000));
    refeynmanGroup.add(refeynmanParameters.incrementalCorrespondenceReduction.set("incremental correspondence reduction", 4, 0.0001, 10));
    refeynmanGroup.add(refeynmanParameters.normalSearchRadius.set("normal search radius", 30, 0.01, 100));
    refeynmanGroup.add(refeynmanParameters.transformationEpsilon.set("transformation epsilon", 0.000001, 0.0, 0.0001));
    refeynmanGroup.add(refeynmanParameters.transformRotationEpsilon.set("rotation epsilon", 0.000001, 0.0, 0.00000001));
    refeynmanGroup.add(refeynmanParameters.save.set("save_settings", false));
    refeynmanGroup.add(refeynmanParameters.load.set("load_settings", false));
    refeynmanGroup.add(refeynmanParameters.reset.set("reset_children", false));

    refeynmanGuiGroup.setup(refeynmanGroup, "refeynman.json");
    ofAddListener(refeynmanGroup.parameterChangedE(), this, &ofApp::refeynmanParametersListener);
    gui.add(&refeynmanGuiGroup);
    refeynmanGuiGroup.minimizeAll();

    const int32_t device_count = k4a::device::get_installed_count();
    for(int i = 0; i < device_count; i++){
        // setup kinects
        auto kirnect = make_shared <ofxDepthDevice::Kinect>();
        kirnect->setup(i, ofxDepthDevice::K4A_DEVICE_CONFIG_OFXK4AMINI_DEFAULT, true);

        string serial_number = kirnect->getSerialNumber();
        ofLogNotice("ofApp::setup") << "loading device (" << kirnect->getSerialNumber() << ")" << endl;

        stringstream device_name;
        int position_index = i;

        string manualGuiFilePath = "calibration/" + serial_number + "/manualAdjustments.json";
        if(ofFile(manualGuiFilePath).exists()){
            ofJson ondiskJson = ofLoadJson(manualGuiFilePath);
            ofJson json;
            for(ofJson::iterator it = ondiskJson.begin(); it != ondiskJson.end(); ++it){
                std::cout << "setup " << __LINE__ << " : " <<  it.key() << " : " << it.value() << "\n";
                if(ofIsStringInString(it.key(), "manual")){
                    position_index = ofToInt(it.value()["position_index"].get <string>());
                    device_name << "device_" << it.value()["position_index"].get <string>();
                    json[(device_name.str() + "_manual")] = it.value();
                    ofSaveJson(manualGuiFilePath, json);
                    break;
                }
            }
        }else{
            device_name << "device_tmp_" << ofToString(i);
        }

//        kirnect->device_index = i;
        cout << "setup kinect " << i <<  " serial " << serial_number << endl;
        kirnects.push_back(kirnect);

        // setup CalibrationHelper
        CalibrationHelper::Helper helper;
        calibrationHelpers.push_back(helper);
        calibrationHelpers[i].setup(serial_number,
                                    CalibrationHelper::CALIBRATIONBOARD_PARAMETERS_A2_MULTI);
        kirnect->setCalibrationExtrinsics(calibrationHelpers[i].commonData.extrinsics.cameraPoseMatrix);
        // load refinement if there
        string refeynmanFilename = "calibration/" + serial_number + "/refeynman.json";
        ofFile refeynmanFile(refeynmanFilename);
        if(refeynmanFile.exists()){
            glm::mat4 refeynmanMatrix;
            CalibrationHelper::CalibratorExtrinsics::loadFromDisk(refeynmanFilename, refeynmanMatrix);
            kirnect->setRefinementExtrinsics(refeynmanMatrix);
        }
        // read intrinsics
//        calibrationHelpers[i].commonData.intrinsics.isCalibrated = kirnect->readIntrinsics(calibrationHelpers[i].commonData.intrinsics.cameraMatrix,
//                                                                                           calibrationHelpers[i].commonData.intrinsics.distCoeffs);

        // set extrinsics
        calibrationHelpers[i].commonData.extrinsics.cameraTransformationMatrix = kirnect->getColorToDepth();
        calibrationHelpers[i].commonData.extrinsics.hasTransformation = false;

        // setup gui
        ofParameterGroup group;
        DeviceParameters dp;

        float range = 3000;
        group.setName(device_name.str());
        group.add(dp.serial.set("serial", serial_number));
        group.add(dp.draw_device.set("draw_device", true));
        group.add(dp.draw_debug.set("draw_debug", true));
        group.add(dp.calibrationMom.set("calibrationMom", false));
        group.add(dp.calibrationChild.set("calibrationChild", false));
        ofAddListener(group.parameterChangedE(), this, &ofApp::deviceParametersListener);
        gui.add(group);
        gui.getGroup(device_name.str()).minimizeAll();
        gui.getGroup(device_name.str()).minimize();

        ofParameterGroup manualGroup;
        manualGroup.setName((device_name.str() + "_manual"));
        manualGroup.add(dp.save_settings.set("save_settings", false));
        manualGroup.add(dp.load_settings.set("load_settings", false));
        manualGroup.add(dp.position_index.set("position_index", position_index, 0, device_count));
        manualGroup.add(dp.center.set("center", glm::vec3(0), glm::vec3(-range), glm::vec3(range)));
        manualGroup.add(dp.centerFine.set("centerFine", glm::vec3(0), glm::vec3(-10), glm::vec3(10)));
        manualGroup.add(dp.position.set("position", glm::vec3(0), glm::vec3(-range), glm::vec3(range)));
        manualGroup.add(dp.positionFine.set("positionFine", glm::vec3(0), glm::vec3(-10), glm::vec3(10)));
        manualGroup.add(dp.orientation.set("orientation", glm::vec3(0), glm::vec3(-360), glm::vec3(360)));
        manualGroup.add(dp.orientationFine.set("orientationFine", glm::vec3(0), glm::vec3(-45), glm::vec3(45)));
        manualGroup.add(dp.cropPosition.set("cropPosition", glm::vec3(0, 0, 120), glm::vec3(-range), glm::vec3(range)));
        manualGroup.add(dp.cropRadius.set("cropRadius", 70, 1, range));
        manualGroup.add(dp.cropHeight.set("cropHeight", 200, 1, range));
        ofAddListener(manualGroup.parameterChangedE(), this, &ofApp::deviceParametersListener);

        shared_ptr <ofxGuiGroup> manualAdjustmentsGuiGroup = make_shared <ofxGuiGroup>();

        manualAdjustmentsGuiGroup->setup(manualGroup, manualGuiFilePath);

        gui.add(manualAdjustmentsGuiGroup.get());
        gui.getGroup(device_name.str() + "_manual").minimizeAll();
        gui.getGroup(device_name.str() + "_manual").minimize();

        manualAdjustments.push_back(manualAdjustmentsGuiGroup);
        //dp.setDeviceCropping(kirnect);

        deviceParameters.push_back(dp);
    }

    ofParameterGroup transportPipelineGroup;
    transportPipelineGroup.setName("transport_pipeline");
    string transportPipelineFilePath = "transport_pipeline.json";
    transportPipelineGroup.add(transportPipeline.pointSize.set("pointSize", 1, 0.5, 20.0));

    transportPipelineGroup.add(transportPipeline.normal_searchRadius.set("normal_SearchRadius", 30.0, 0.01, 100.0));
    transportPipelineGroup.add(transportPipeline.normal_recalculate.set("normal_recalculate", false));

    transportPipelineGroup.add(transportPipeline.outlier_kMean.set("outlier_kMean", 50.0, 1.0, 1000.0));
    transportPipelineGroup.add(transportPipeline.outlier_mulTresh.set("outlier_mulTresh", 1.0, 0.01, 10.0));
    transportPipelineGroup.add(transportPipeline.outlier_recalculate.set("outlier_recalculate", false));

    transportPipelineGroup.add(transportPipeline.lods_levels_string);
    transportPipelineGroup.add(transportPipeline.lods_recalculate.set("lods_recalculate", false));
    transportPipelineGuiGroup.setup(transportPipelineGroup, transportPipelineFilePath);
    ofAddListener(transportPipelineGroup.parameterChangedE(), this, &ofApp::transportPipelineParametersListener);
    gui.add(&transportPipelineGuiGroup);

    // needs listener to fire
    transportPipeline.lods_levels_string.set("lods_levels", "1,2,8,64,256,1024");

    if(ofFile(transportPipelineFilePath).exists()){
        transportPipelineGuiGroup.loadFromFile(transportPipelineFilePath);
    }else{
        transportPipelineGuiGroup.saveToFile(transportPipelineFilePath);
    }

    for(int i = 0; i < kirnects.size(); i++){
        string manualGuiFilePath = "calibration/" + kirnects[i]->getSerialNumber() + "/manualAdjustments.json";
        if(ofFile(manualGuiFilePath).exists()){
            cout << "loading from file " << __LINE__ << " |" << manualGuiFilePath << "|" << endl;
            manualAdjustments[i]->loadFromFile(manualGuiFilePath);
            cout << "loaded from file " << __LINE__ << " |" << manualGuiFilePath << "|" << endl;
            //deviceParameters[i].setDeviceCropping(kirnects[i]);
        }
    }

    ofParameterGroup visualParametersGuiGroup;
    visualParametersGuiGroup.setName("visual_parameters");

    VisualParameterGroup vpg_mirror;
    vpg_mirror.setup("mirror");
    visualParameterGroups["mirror"] = vpg_mirror;
    //visualParameterGroups.insert(make_pair("mirror", vpg_mirror));
    for(auto & stageStringKP : transportPipeline.transportStageString){
        string stageString = stageStringKP.second;
        cout << "stageString: " << stageString << endl;
        VisualParameterGroup vpg;
        vpg.setup(stageString);
        visualParameterGroups[stageString] = vpg;
        //visualParameterGroups.insert(std::make_pair(stageString, vpg));
        //visualParameterGroups[stageString]->group.setName(stageString);
    }

    for(auto & v : visualParameterGroups){
        visualParametersGuiGroup.add(v.second.group);
    }
    gui.add(visualParametersGuiGroup);
    gui.minimizeAll();

    string globalCalibrationFilePath = "calibration/global_calibration.json";
    if(ofFile(globalCalibrationFilePath).exists()){
        cout << "loading from file " << __LINE__ << endl;
        appGlobalCalibrationGuiGroup.loadFromFile(globalCalibrationFilePath);
        ofLogNotice() << "loaded " << globalCalibrationFilePath;
    }

    string refeynmanFilePath = "refeynman.json";
    if(ofFile(refeynmanFilePath).exists()){
        cout << "loading from file " << __LINE__ << endl;
        refeynmanGuiGroup.loadFromFile(refeynmanFilePath);
        ofLogNotice() << "loaded " << refeynmanFilePath;
    }

    kinect_azure_model.loadModel("assets/kinect_azure_model.obj", true);
    float scale = 1;
    kinect_azure_model.setScale(scale, scale, scale);
    kinect_azure_model.setScaleNormalization(false);
    kinect_azure_model.calculateDimensions();

    mainShader.load("shader/normals.vert", "shader/normals.frag");

    mainScreen = make_shared <Screen>();
    mainScreen->fbo.allocate(MAIN_WIDTH, MAIN_HEIGHT, GL_RGBA);
    mainScreen->fbo.begin();
    ofClear(0);
    ofBackground(ofColor::grey);
    mainScreen->fbo.end();

    mainScreen->cam.setPosition(2000, 2000, 2000);
    mainScreen->cam.lookAt(glm::vec3(0, 0, 0));

    if(ofFile("user_camera")){
        ofxLoadCamera(mainScreen->cam, "user_camera");
    }

    for(int i = 0; i < N_SPLIT_SCREENS; i++){
        screens.push_back(make_shared <Screen>());

        ofFbo & fbo = screens[i]->fbo;
        fbo.allocate(FBO_WIDTH, FBO_HEIGHT, GL_RGBA);
        fbo.begin();
        ofClear(0);
        ofBackground(ofColor::grey);
        fbo.end();

        ofEasyCam & cam = screens[i]->cam;
        float longitude = ((i + 1.0f) / N_SPLIT_SCREENS) * 360.0f;
        cam.setNearClip(0);
        cam.setFarClip(10000000);
        cam.orbitDeg(longitude, 0, 2000, glm::vec3());
        cam.disableMouseInput();
        cam.disableMouseMiddleButton();
    }
    screens[0]->cam.enableMouseInput();
    screens[0]->cam.enableMouseMiddleButton();

    //glEnable(GL_PROGRAM_POINT_SIZE);
    //transportPipeline.setup(kirnects.size(), "default");
    ofLogNotice("ofApp::setup") << "done";
}

void ofApp::transportPipelineParametersListener(ofAbstractParameter & e){
    if(e.getName() == "normalSearchRadius"){
        transportPipeline.normal_searchRadius = e.cast <float>();
    }
    if(e.getName() == "pointSize"){
        transportPipeline.pointSize = e.cast <float>();
    }
    if(e.getName() == "normal_recalculate"){
        bool v = e.cast <bool>();
        cout << (v ? "recalculate normals true" : "recalculate normals false") << endl;
        transportPipeline.setRecalculation(e.cast <bool>(), TransportPipeline::CalculatingNormals);
    }
    if(e.getName() == "outlier_recalculate"){
        bool v = e.cast <bool>();
        cout << (v ? "recalculate outlier true" : "recalculate outlier false") << endl;
        transportPipeline.setRecalculation(e.cast <bool>(), TransportPipeline::RemoveOutlier);
    }
    if(e.getName() == "lods_levels"){
        string v = e.cast <string>();
        vector <string> levels_str = ofSplitString(v, ",", true, true);
        vector <int> levels;
        for(auto & level_str: levels_str){
            levels.push_back(ofToInt(level_str));
        }
        transportPipeline.lods_levels = levels;
    }
    if(e.getName() == "lods_recalculate"){
        bool v = e.cast <bool>();
        cout << (v ? "recalculate lods true" : "recalculate outlier false") << endl;
        transportPipeline.setRecalculation(e.cast <bool>(), TransportPipeline::CreateLODs);
    }
}

void ofApp::appActionListener(int & e){
    ofLogNotice("appActionListener") << "appAction has been set to " << appActionStrings[e] << "(" << e << ")";
    if(!appActionGroup.get <bool>(appActionStrings[e]).get()){
        ofLogNotice("appActionListener") << "button is false";
        for(int i = 0; i < appActionGroup.size(); i++){
            appActionGroup.get <bool>(appActionStrings[i]).set(i == e);
        }
    }else{
        ofLogNotice("appActionListener") << "button is true";
    }
}

void ofApp::appActionGroupListener(ofAbstractParameter & e){
    if(appActionGroup.get <bool>(e.getName()).get()){
        ofLogNotice("appActionGroupListener") << e.getName() << " is true" << endl;
        AppAction newAction;
        for(int i = 0; i < appActionGroup.size(); i++){
            if(appActionStrings[i] != e.getName()){
                appActionGroup.get <bool>(appActionStrings[i]).set(false);
            }else{
                newAction = AppAction(i);

                // things that should happen only once when a new action is called
                if(newAction == APP_ACTION_SAVE_MODEL){
                    appParameters.updateDevices.set(false);

                    string name = ofGetTimestampString("%Y.%m.%d_%H:%M:%S");
                    transportPipeline.setup(kirnects.size(), name);
                    for(auto & k: kirnects){
                        k->requestScan();
                    }
                }
                if(newAction == APP_ACTION_QUIT){
                    exit();
                }
            }
        }
        appAction.set(newAction);
        ofLogNotice("appActionGroupListener") << " appAction should be set " << appActionStrings[int(newAction)] << endl;
    }
}

void ofApp::doCameraAction(const CameraAction & cameraAction) {
    float currentDistance = mainScreen->cam.getDistance();
         mainScreen->cam.disableInertia();

    switch(cameraAction){
     case (CAMERA_ACTION_DEFAULT): {
         mainScreen->cam.disableOrtho();
         mainScreen->cam.setPosition(4000,4000,4000);
         mainScreen->cam.lookAt(glm::vec3(0),glm::vec3(1,0,0));
         break;
     }
     case (CAMERA_ACTION_PERSPECTIVE): {
         mainScreen->cam.disableOrtho();
         mainScreen->cam.setScrollSensitivity(1);
         break;
     }
     case (CAMERA_ACTION_ORTHO): {
         mainScreen->cam.enableOrtho();
         mainScreen->cam.setScrollSensitivity(100);
         break;
     }
     case (CAMERA_ACTION_ALIGN_X): {
         mainScreen->cam.setPosition(currentDistance,0,0);
         mainScreen->cam.lookAt(glm::vec3(0));
         break;
     }
     case (CAMERA_ACTION_ALIGN_Y): {
         mainScreen->cam.setPosition(0,currentDistance,0);
         mainScreen->cam.lookAt(glm::vec3(0));
         break;
     }
     case (CAMERA_ACTION_ALIGN_Z): {
         mainScreen->cam.setPosition(0,0,currentDistance);
         mainScreen->cam.lookAt(glm::vec3(0));
         break;
     }
     case (CAMERA_ACTION_ALIGN_FLIP): {
         mainScreen->cam.setPosition(mainScreen->cam.getPosition() * -1);
         mainScreen->cam.lookAt(glm::vec3(0));
         break;
     }
     default: {
     }
    }
}

void ofApp::cameraActionGroupListener(ofAbstractParameter & e){
    int pos = positionInVector(cameraActionStrings, e.getName());
    CameraAction cameraAction = CameraAction(pos);
    doCameraAction(cameraAction);
}

void ofApp::appParametersListener(ofAbstractParameter & e){
    if(e.getName() == "quit"){
        exit();
    }
    if(e.getName() == "update_devices"){
        bool v = e.cast <bool>();
        for(auto & k : kirnects){
            k->setUpdating(v);
        }
    }
    if(e.getName() == "draw_devices"){
        bool v = e.cast <bool>();
        for(auto & p : deviceParameters){
            p.draw_device.set(v);
        }
    }
    if(e.getName() == "draw_debug"){
        bool v = e.cast <bool>();
        for(auto & p : deviceParameters){
            p.draw_debug.set(v);
        }
    }
    if(e.getName() == "cropping"){
        bool v = e.cast <bool>();
        if(v){
            for(int i = 0; i < deviceParameters.size(); i++){
                deviceParameters[i].setDeviceCropping(kirnects[i]);
            }
        }else{
            for(int i = 0; i < deviceParameters.size(); i++){
                ofCylinderPrimitive cropping;
                cropping.resetTransform();
                cropping.setResolutionHeight(1);
                cropping.setResolutionRadius(16);
                cropping.setHeight(8000);
                cropping.setRadius(8000);
//                cropping.setPosition(appParameters.center.get());
                cropping.rotateDeg(90, 0, 0, 1);
//                cropping.rotate(appParameters.orientation.get());
                kirnects[i]->setLocalCropping(cropping);
            }
        }
    }
    if(e.getName() == "save_model"){
        ofLogNotice("ofApp::events") << " received save model" << endl;
        bool v = e.cast <bool>();
        if(v){
            appAction.set(APP_ACTION_SAVE_MODEL);
        }
    }
    const vector <string> extrinsics{"center",  "orientation"};
    if(find(extrinsics.begin(), extrinsics.end(), e.getName()) != extrinsics.end()){
        for(int i = 0; i < deviceParameters.size(); i++){
            deviceParameters[i].setDeviceExtrinsics(kirnects[i], appParameters);
        }
    }
    if(e.getName() == "save_global_calibration"){
        bool v = e.cast <bool>();
        if(v){
            appParameters.saveGlobalCalibration.set(false);
            appGlobalCalibrationGuiGroup.saveToFile("calibration/global_calibration.json");
        }
    }
    if(e.getName() == "save_all_calibration"){
        bool v = e.cast <bool>();
        if(v){
            appParameters.saveCalibration.set(false);
            appParameters.saveGlobalCalibration.set(false);
            appGlobalCalibrationGuiGroup.saveToFile("calibration/global_calibration.json");

            refeynmanParameters.save.set(false);
            refeynmanGuiGroup.saveToFile("refeynman.json");

            for(int i = 0; i < kirnects.size(); i++){
                string subDirectory = "calibration/" + kirnects[i]->getSerialNumber();
                glm::mat4 refeynmanMatrix = kirnects[i]->getRefinementExtrinsics();
                CalibrationHelper::CalibratorExtrinsics::saveToDisk((subDirectory + "/refeynman.json"), refeynmanMatrix);

                deviceParameters[i].save_settings.set(false);
                manualAdjustments[i]->saveToFile((subDirectory + "/manualAdjustments.json"));

                glm::mat4 extrinsicsMatrix = kirnects[i]->getCalibrationExtrinsics();
                CalibrationHelper::CalibratorExtrinsics::saveToDisk((subDirectory + "/extrinsics.json"), extrinsicsMatrix);

                calibrationHelpers[i].saveIntrinsicsToDisk("intrinsics.yml");
            }

            ofxSaveCamera(mainScreen->cam, "user_camera");
        }
    }
}

void ofApp::deviceParametersListener(ofAbstractParameter & e){
    int deviceIndex;     // must be found!

    { // scope to find deviceIndex
        int hierarchy_device = 1;
        string deviceString = e.getGroupHierarchyNames()[hierarchy_device];
        string deviceIndexString = deviceString;
        ofStringReplace(deviceIndexString, "device_", "");
        ofStringReplace(deviceIndexString, "_manual", "");
        int position_index = ofToInt(deviceIndexString);
        for(int i = 0; i < int(deviceParameters.size()); i++){
            if(deviceParameters[i].position_index.get() == position_index){
                deviceIndex = i;
            }
        }
    }


    const vector <string> extrinsics{"center", "position", "orientation", "centerFine", "positionFine", "orientationFine"};
    const vector <string> localCropping{"cropPosition", "cropHeight", "cropRadius"};

    if(find(extrinsics.begin(), extrinsics.end(), e.getName()) != extrinsics.end()){
        deviceParameters[deviceIndex].setDeviceExtrinsics(kirnects[deviceIndex], appParameters);
    }else if(find(localCropping.begin(), localCropping.end(), e.getName()) != localCropping.end()){
        deviceParameters[deviceIndex].setDeviceCropping(kirnects[deviceIndex]);
    }else if(e.getName() == "calibrationMom"){
        if(e.cast <bool>()){
            deviceParameters[deviceIndex].calibrationChild.set(false);
            for(int i = 0; i < kirnects.size(); i++){
                if(!e.isReferenceTo(deviceParameters[i].calibrationMom)){
                    deviceParameters[i].calibrationMom.set(false);
                }
            }
        }
    }else if(e.getName() == "calibrationChild"){
        if(e.cast <bool>()){
            deviceParameters[deviceIndex].calibrationMom.set(false);
        }
    }else if(e.getName() == "save_settings"){
        if(e.cast <bool>() && ofGetFrameNum() > 30){
            deviceParameters[deviceIndex].save_settings.set(false);
            string filePath = "calibration/" + kirnects[deviceIndex]->getSerialNumber() + "/manualAdjustments.json";
            manualAdjustments[deviceIndex]->saveToFile(filePath);
        }
    }else if(e.getName() == "load_settings"){
        if(e.cast <bool>() && ofGetFrameNum() > 30){
            string filePath = "calibration/" + kirnects[deviceIndex]->getSerialNumber() + "/manualAdjustments.json";
            if(ofFile(filePath).exists()){
                cout << "loading from file " << __LINE__ << endl;
                manualAdjustments[deviceIndex]->loadFromFile(filePath);
            }
            deviceParameters[deviceIndex].load_settings.set(false);
            deviceParameters[deviceIndex].save_settings.set(false);
        }
    }
}
void ofApp::refeynmanParametersListener(ofAbstractParameter & e){
    if(e.getName() == "save_settings"){
        if(e.cast <bool> ()){
            refeynmanParameters.save.set(false);
            refeynmanGuiGroup.saveToFile("refeynman.json");
        }

    }
    if(e.getName() == "load_settings"){
        if(e.cast <bool> ()){
            cout << "loading from file " << __LINE__ << endl;
            refeynmanGuiGroup.loadFromFile("refeynman.json");
            refeynmanParameters.load.set(false);
            refeynmanParameters.save.set(false);
        }
    }
    if(e.getName() == "reset_children"){
        for(int i = 0; i < (int)kirnects.size(); i++){
            if(deviceParameters[i].calibrationChild.get()){
                glm::mat4 m = glm::mat4(1);
                kirnects[i]->calibration_mtx.lock();
                kirnects[i]->refinementExtrinsics = m;
                kirnects[i]->calibration_mtx.unlock();
            }
        }
    }
}

void DeviceParameters::setDeviceExtrinsics(shared_ptr <ofxDepthDevice::Kinect> & device, const AppParameters & appParameters){
    device->calibration_mtx.lock();
    ofNode & e = device->manualExtrinsics;
//    e.setPosition(0, 0, 0);
//    e.setOrientation(glm::quat());
    e.resetTransform();

    glm::mat4 transformation; // your transformation matrix.

    transformation = glm::translate(transformation, center.get());
    transformation = glm::translate(transformation, centerFine.get());

//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().x, glm::vec3(1, 0, 0));
//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().y, glm::vec3(0, 1, 0));
//    transformation = glm::rotate(transformation, device->imu.getOrientationEulerRad().z, glm::vec3(0, 0, 1));

    transformation = glm::rotate(transformation, glm::radians(orientation.get().x), glm::vec3(1, 0, 0));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().x), glm::vec3(1, 0, 0));
    transformation = glm::rotate(transformation, glm::radians(orientation.get().y), glm::vec3(0, 1, 0));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().y), glm::vec3(0, 1, 0));
    transformation = glm::rotate(transformation, glm::radians(orientation.get().z), glm::vec3(0, 0, 1));
    transformation = glm::rotate(transformation, glm::radians(orientationFine.get().z), glm::vec3(0, 0, 1));

    transformation = glm::translate(transformation, -center.get());
    transformation = glm::translate(transformation, -centerFine.get());
    transformation = glm::translate(transformation, position.get());
    transformation = glm::translate(transformation, positionFine.get());

    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(transformation, scale, rotation, translation, skew, perspective);
//    e.setPosition(translation);
    glm::vec3 newPosition = translation + appParameters.center.get(); //+ center.get() + centerFine.get();
    // TODO: implement rotation
//    glm::quat newRotation = glm::rotate(rotation, glm::radians(appParameters.orientation.get()));
    e.setPosition(newPosition);
    e.setOrientation(rotation);
    device->calibration_mtx.unlock();
//    device->applyExtrinsics();

    cout << "set device extrinsics " << device->serial_number << " " << __LINE__ << endl;
}
void DeviceParameters::setDeviceCropping(shared_ptr <ofxDepthDevice::Kinect> & device){
    device->localCropping.resetTransform();
    device->localCropping.setResolutionHeight(1);
    device->localCropping.setResolutionRadius(16);
    device->localCropping.setHeight(cropHeight.get());
    device->localCropping.setRadius(cropRadius.get());
    device->localCropping.setPosition(cropPosition.get());
    device->localCropping.rotateDeg(90, 0, 0, 1);
    cout << "set device cropping " << device->serial_number << endl;
}

void ofApp::drawDebug(int deviceIndex){
    auto & device = kirnects[deviceIndex];

    bool isMom = deviceParameters[deviceIndex].calibrationMom.get();
    bool isChild = deviceParameters[deviceIndex].calibrationChild.get();

    float dfrgb = 44.0f / 255.0f;
    ofColor df = ofFloatColor(dfrgb);
    ofFloatColor color = isMom ? ofColor::red : isChild ? ofColor::green : df;
    float lineWidth = isMom ? 4 : isChild ? 4 : 2;

    gui.getGroup("device_" + ofToString(deviceIndex)).setHeaderBackgroundColor(color);
    gui.getGroup("device_" + ofToString(deviceIndex) + "_manual").setHeaderBackgroundColor(color);
    if(isChild){
        gui.getGroup("device_" + ofToString(deviceIndex)).setTextColor(ofColor::red);
        gui.getGroup("device_" + ofToString(deviceIndex) + "_manual").setTextColor(ofColor::red);
    }else if(isMom){
        gui.getGroup("device_" + ofToString(deviceIndex)).setTextColor(ofColor::green);
        gui.getGroup("device_" + ofToString(deviceIndex) + "_manual").setTextColor(ofColor::green);
    }else{
        gui.getGroup("device_" + ofToString(deviceIndex)).setTextColor(ofColor::white);
        gui.getGroup("device_" + ofToString(deviceIndex) + "_manual").setTextColor(ofColor::white);
    }

    ofEnableLighting();
    ofLight light;
    light.setPosition(0, 0, 0);
    light.enable();

    ofPushMatrix();
    glm::mat4 matrix = device->getExtrinsics();
    ofMultMatrix(matrix);

    kinect_azure_model.drawFaces();
    ofDisableLighting();

    ofPushStyle();
    ofNoFill();
    ofSetLineWidth(lineWidth);
    ofSetColor(color);
    ofDrawBox(0, 0, -125.4 / 2, 103, 39, 125.4);
    ofFill();
    ofDrawArrow(
        glm::vec3(0, -39 / 2, 0),
        glm::vec3(0, -(10 + 39 / 2), 0),
        lineWidth * 2.5
        );
    ofPopStyle();

    ofPopMatrix();

    ofPushMatrix();
    glm::vec3 cameraPosition = matrix * glm::vec4(0, 0, 0, 1);
    ofTranslate(cameraPosition);
    ofDisableDepthTest();
    ofDrawBitmapString(ofToString(deviceParameters[deviceIndex].position_index.get()), 0, 0);
    ofEnableDepthTest();
    ofPopMatrix();
    ofDisableLighting();

    if(appParameters.cropping.get()){
        ofPushStyle();
        ofSetColor(color);
        ofSetLineWidth(lineWidth);
        device->getLocalCropping().drawWireframe();
        ofPopStyle();
    }

}

void ofApp::changedPosition(glm::vec3 & e){
    cout << e.x << endl;
    kirnects[0]->manualExtrinsics.setPosition(e);
//do something
}

//--------------------------------------------------------------
void ofApp::update(){
    OFX_PROFILER_FUNCTION();
    uint64_t millis = ofGetSystemTimeMillis();

    {
        OFX_PROFILER_SCOPE("update 1");
        for(int i = 0; i < (int)kirnects.size(); i++){
            auto & kirnect = kirnects[i];
            kirnect->update();
        }

        if(appAction.get() == APP_ACTION_GENERATE_MARKER){
            for(int i = 0; i < (int)kirnects.size(); i++){
                calibrationHelpers[i].generateBoard();
            }
        }

        if(appAction.get() == APP_ACTION_INTRINSIC_CALIBRATION_COLLECT){
            for(int i = 0; i < (int)kirnects.size(); i++){
                auto & kirnect = kirnects[i];
                auto fucker = kirnect->getColorImage();
                if(ofGetFrameNum() % 15 == 0 && deviceParameters[i].draw_device.get() && fucker.isAllocated()){
                    fucker.setUseTexture(true);
                    fucker.update();
                    calibrationHelpers[i].intrinsicsDetectBoard(fucker.getPixels());
                    cout << "detected board " << kirnect->getSerialNumber() << " :::::: " << calibrationHelpers[i].commonData.id << endl;
                }
//            if(calibrationHelper.calibratorIntrinsics.nDetectedFrames >= 142){
//                appAction.set(APP_ACTION_INTRINSIC_CALIBRATION_CALIBRATE);
//            }
            }
        }

        if(appAction.get() == APP_ACTION_INTRINSIC_CALIBRATION_CALIBRATE){
            for(int i = 0; i < (int)kirnects.size(); i++){
                auto & kirnect = kirnects[i];
                if(deviceParameters[i].draw_device.get()){
                    calibrationHelpers[i].intrinsicsCalibrateCamera();
                    cout << "calibrated camera " << kirnect->getSerialNumber() << " :::::: " << calibrationHelpers[i].commonData.id << endl;
                    cout << "intrinsics " << calibrationHelpers[i].commonData.intrinsics.cameraMatrix;
                }
            }
            appAction.set(APP_ACTION_IDLE);
        }
    }

    {
        OFX_PROFILER_SCOPE("update 2");
        if(appAction.get() == APP_ACTION_EXTRINSIC_CALIBRATION_POSE){
            int momIndex = -1;
            glm::mat4 oldMomMatrix = glm::mat4(1);
            vector <int> childrenIndices;
            for(int i = 0; i < (int)kirnects.size(); i++){
                if(deviceParameters[i].calibrationMom.get()){
                    momIndex = i;
                    oldMomMatrix = calibrationHelpers[i].commonData.extrinsics.cameraPoseMatrix;
                }else if(deviceParameters[i].calibrationChild.get()){
                    childrenIndices.push_back(i);
                }
            }
            if(momIndex == -1 || childrenIndices.size() == 0){
                cout << "no mom or children " << __LINE__ << endl;
            }else{
                for(int i = 0; i < (int)kirnects.size(); i++){
                    bool calibrate = (existsInVector(childrenIndices, i) || i == momIndex);
                    if(calibrate){
                        auto & kirnect = kirnects[i];
                        auto fucker = kirnect->getColorImage();
                        if(fucker.isAllocated()){
                            fucker.setUseTexture(true);
                            fucker.update();
                            calibrationHelpers[i].extrinsicsGetCameraPose(fucker.getPixels());
                        }
                    }
                }

                // where does mom go?
                glm::mat4 tmpMomMatrix = calibrationHelpers[momIndex].commonData.extrinsics.cameraPoseMatrix;
                glm::mat4 invertedTmpMomMatrix = glm::inverse(tmpMomMatrix);

                glm::mat4 goHomeMatrix = oldMomMatrix * invertedTmpMomMatrix;
                // the new mom matrix shall be the same as oldMomMatrix!!!!!
                calibrationHelpers[momIndex].commonData.extrinsics.cameraPoseMatrix = goHomeMatrix * tmpMomMatrix;

                // set the mom device
                kirnects[momIndex]->setCalibrationExtrinsics(calibrationHelpers[momIndex].commonData.extrinsics.cameraPoseMatrix);

                // immediately save the extrinsics, why not
                string momExtrinsicsFileName = calibrationHelpers[momIndex].commonData.saveLoadPath + "/extrinsics.json";
                if(!CalibrationHelper::CalibratorExtrinsics::saveToDisk(momExtrinsicsFileName, calibrationHelpers[momIndex].commonData.extrinsics.cameraPoseMatrix)){
                    ofLogError("whoops, couldn't save") << "the extrinsic calibration " << __LINE__ << endl;
                }

                for(auto & i: childrenIndices){
                    auto & kirnect = kirnects[i];

                    glm::mat4 tmpChildMatrix = calibrationHelpers[i].commonData.extrinsics.cameraPoseMatrix;
                    calibrationHelpers[i].commonData.extrinsics.cameraPoseMatrix = goHomeMatrix * tmpChildMatrix;

                    kirnect->setCalibrationExtrinsics(calibrationHelpers[i].commonData.extrinsics.cameraPoseMatrix);

                    string extrinsicsFileName = calibrationHelpers[i].commonData.saveLoadPath + "/extrinsics.json";
                    if(!CalibrationHelper::CalibratorExtrinsics::saveToDisk(extrinsicsFileName, calibrationHelpers[i].commonData.extrinsics.cameraPoseMatrix)){
                        ofLogError("whoops, couldn't save") << "the extrinsic calibration " << __LINE__ << endl;
                    }
//                string filename = "images/" + kirnect->serial_number + "_" + ofGetTimestampString();
//                fucker->save(filename + ".png");
//                calibrationHelpers[i].commonData.detectedBoardExtrinsics.save(filename + ".DETECT.png");
                }
            }
            appAction.set(APP_ACTION_IDLE);
        }
    }

    {
        OFX_PROFILER_SCOPE("update 3");
        if(appAction.get() == APP_ACTION_EXTRINSIC_CALIBRATION_REFEYNMAN){
            int momIndex = -1;
            int childIndex;
            for(int i = 0; i < (int)kirnects.size(); i++){
                if(deviceParameters[i].calibrationMom.get()){
                    momIndex = i;
                }else if(deviceParameters[i].calibrationChild.get()){
                    childIndex = i;
                }
            }
            if(momIndex == -1 || childIndex == -1){
                cout << "no mom or child " << __LINE__ << endl;
            }else{
                const auto & momMesh = kirnects[momIndex]->getVboMesh();
                const auto & childMesh = kirnects[childIndex]->getVboMesh();
                glm::mat4 stepMatrix;
                Refeynman::pairAlign(momMesh,
                                     childMesh,
                                     refeynmanParameters,
                                     stepMatrix);
                kirnects[childIndex]->setRefinementExtrinsics(stepMatrix);
                glm::mat4 collectedMatrix = kirnects[childIndex]->getRefinementExtrinsics();
                CalibrationHelper::CalibratorExtrinsics::saveToDisk(("calibration/" + kirnects[childIndex]->getSerialNumber() + "/refeynman.json"), collectedMatrix);
            }
            appAction.set(APP_ACTION_IDLE);
        }
    }
    {
        OFX_PROFILER_SCOPE("update 4");
        if(appAction.get() == APP_ACTION_SAVE_MODEL){

            saviour.update();
            saviourLODs.update();
            transportPipeline.update();

            // only save if nothing is being recalculated
            if(!transportPipeline.recalculating.load()){
                if(transportPipeline.prepared.load() && !transportPipeline.saved.load()){
                    if(saviour.is_ready.load() && !saviour.is_done.load()){
                        auto output = transportPipeline.getOutput();
                        vector <ofVboMesh> meshes;

                        for(auto & m : output.meshes){
                            meshes.push_back(m);
                        }
                        saviour.save(meshes,
                                     output.names,
                                     output.textures,
                                     transportPipeline.scanName);
                    }else if(saviour.is_done.load() && !transportPipeline.saved.load()){
                        transportPipeline.saved.store(true);
                        if(transportPipeline.savedLODs.load()){
                            ofLogNotice() << "saved scan LODs" << __LINE__ << endl;
                        }
                    }
                }
                if(transportPipeline.preparedLODs.load() && !transportPipeline.savedLODs.load()){
                    if(saviourLODs.is_ready.load() && !saviourLODs.is_done.load()){
                        auto lodput = transportPipeline.getLODput();
                        vector <ofVboMesh> meshes;
                        for(auto & m : lodput.meshes){
                            meshes.push_back(m);
                        }
                        saviourLODs.saveLODs(meshes,
                                             lodput.names,
                                             transportPipeline.scanName);
                    }else if(saviourLODs.is_done.load() && !transportPipeline.savedLODs.load()){
                        transportPipeline.savedLODs.store(true);
                        if(transportPipeline.saved.load()){
                            ofLogNotice() << "saved scan" << __LINE__ << endl;
                        }
                    }
                }
            }

            switch(transportPipeline.getStage()){
             case TransportPipeline::Collecting: {
                 for(int i = 0; i < (int)kirnects.size(); i++){
                     ofVboMesh mesh;
                     ofImage image;
                     if(kirnects[i]->getScan(mesh, image)){
                         ofPixels pixels = image.getPixels();
                         if(transportPipeline.input(mesh, pixels, kirnects[i]->getSerialNumber(), kirnects[i]->getExtrinsics())){
                             // flash lights, when all scans are made
                             ofLogNotice() << "scan made " << __LINE__ << endl;

                         }
                         ofLogNotice() << "put scan " << __LINE__ << endl;
                     }
                 }
                 break;
             }

             case TransportPipeline::CalculatingNormals: {
                 break;
             }

             case TransportPipeline::Idle: {
                 saviour.is_done.store(false);
                 saviourLODs.is_done.store(false);
                 appParameters.saveModel.set(false);
                 appParameters.updateDevices.set(true);
                 showMirror = true;
                 transportPipeline.setStage(TransportPipeline::Empty);
                 appAction.set(APP_ACTION_IDLE);
                 break;
             }

             default: {
                 break;
             }
            }
        }
    }

    ofColor randomColor = ofColor(
        ofRandom(0, 25),
        ofRandom(0, 25),
        ofRandom(0, 25)
        );

    mainScreen->fbo.begin();

    if(transportPipeline.getStage() <= TransportPipeline::Collecting){
        if(!showMirror){
            appParameters.updateDevices.set(true);
            showMirror = true;
        }
    }else{
        appParameters.updateDevices.set(false);
        if(showMirror){
            showMirror = false;
        }
    }

    string currentVisualParameterString = showMirror ? "mirror" : transportPipeline.getStageString();
    auto & currentVisualParameters = visualParameterGroups.at(currentVisualParameterString);
    visualParameters.mix(currentVisualParameters);

    ofEnableDepthTest();
    mainScreen->cam.begin();
    ofPushStyle();
    ofSetColor(255, 255, 255, 255);
    ofBackground(visualParameters.backgroundColor);

    //mainShader.begin();
    //mainShader.setUniform1f("time", millis * 0.001f);
    //visualParameters.setShader(mainShader);
    if(showMirror){
        for(int i = 0; i < kirnects.size(); i++){
            auto & kirnect = kirnects[i];
            auto & p = deviceParameters[i];

            if(p.draw_debug.get()){
                drawDebug(i);
            }

            if(p.draw_device.get()){
                glPointSize(1.0f);
                // possibly change color with mainShader?
                kirnect->draw();
            }
        }
    }else{   // show last scan
        ofSetColor(255, 255, 255, 255);
        transportPipeline.draw(millis, mainShader);
    }
    //mainShader.end();
    if(appParameters.drawDebug.get() && visualParameters.drawGrid){
        drawOurGrid(500, 5, visualParameters);
    }
    ofPopStyle();
    mainScreen->cam.end();
    ofDisableDepthTest();
    for(int repeat = 0; repeat < 12; repeat++){
        ofDrawBitmapStringHighlight(
            ("stage: " + currentVisualParameterString),
            200, 600 + repeat * 30);
    }

    mainScreen->fbo.end();

    {
        OFX_PROFILER_SCOPE("update draw split screens");
        for(int i = 0; i < N_SPLIT_SCREENS; i++){

            ofFbo & fbo = screens[i]->fbo;
            ofEasyCam & cam = screens[i]->cam;

            float speed = 0.042;
            if(i == 0){
                long double & rotationTime = visualParameters.rotationTimes[i];
                rotationTime += speed * visualParameters.rotationSpeed.x;
                float distance = visualParameters.distance;

                glm::vec3 pos;
                pos.x = cos(rotationTime) * distance;
                pos.y = 0;
                pos.z = sin(rotationTime) * distance;
                cam.setPosition(pos);
                cam.lookAt(glm::vec3(0), glm::vec3(pos.z, 0, 0));
            }else if(i == 1){
                long double & rotationTime = visualParameters.rotationTimes[i];
                rotationTime += speed * visualParameters.rotationSpeed.y;
                float distance = visualParameters.distance;

                glm::vec3 pos;
                pos.x = 0;
                pos.y = cos(rotationTime) * distance;
                pos.z = sin(rotationTime) * distance;
                cam.setPosition(pos);
                cam.lookAt(glm::vec3(0), glm::vec3(1, 0, 0));
            }else if(i == 2){
                long double & rotationTime = visualParameters.rotationTimes[i];
                rotationTime += speed * visualParameters.rotationSpeed.z;
                float distance = visualParameters.distance;

                glm::vec3 pos;
                pos.x = cos(rotationTime) * distance;
                pos.y = sin(rotationTime) * distance;
                pos.z = 0;
                cam.setPosition(pos);
                cam.lookAt(glm::vec3(0), glm::vec3(0, pos.x, 0));
            }

            fbo.begin();
            if(saviour.is_saving.load()){
                float a = visualParameters.backgroundColor.a;
                ofBackground(randomColor);
                ofPushStyle();
                ofSetColor(visualParameters.backgroundColor);
                ofRectangle(0, 0, fbo.getWidth(), fbo.getHeight());
                ofPopStyle();
            }else{
                ofBackground(visualParameters.backgroundColor);
            }
            cam.begin();
            ofPushMatrix();
            ofScale(FBO_AA);
            ofEnableDepthTest();
            if(showMirror){
                int deviceIndex = 0;
                mainShader.begin();
                mainShader.setUniform1f("time", millis * 0.001f);
                //mainShader.setUniform1f("normalMix", 0);         //sin(millis * 0.001f) * 0.5 + 0.5);
                visualParameters.setShader(mainShader);
                for(auto & kirnect : kirnects){
                    auto & p = deviceParameters[deviceIndex];
                    if(p.draw_device.get()){
                        glPointSize(2.0f);
                        kirnect->draw(false);
                    }
                    deviceIndex++;
                }

                mainShader.end();
            }else{
                //ofBackground(ofColor::white);
                mainShader.begin();
                mainShader.setUniform1f("time", millis * 0.001f);
                visualParameters.setShader(mainShader);
                transportPipeline.draw(millis, mainShader);
                mainShader.end();
            }
            drawOurGrid(500, 5, visualParameters);
            ofDisableDepthTest();
            ofPopMatrix();
            cam.end();

            fbo.end();
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    OFX_PROFILER_FUNCTION();
    mainScreen->fbo.draw(0, 0, ofGetWidth(), ofGetHeight());
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::exit(){
    for(int i = 0; i < kirnects.size(); i++){
        kirnects[i]->close();
    }
    ofExit(0);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    cout << "KEY PRESSED " << key << endl;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if(key == 'l'){
        ofLogNotice("TransportPipeline::keyReleased")
            << "reloading mainShader";

        mainShader.load("shader/normals.vert",
                        "shader/normals.frag");
    }
    transportPipeline.keyReleased(key);
    if (key == 48 + 7) {
        doCameraAction(CAMERA_ACTION_ALIGN_Z);
    } else if (key == 48 + 1) {
        doCameraAction(CAMERA_ACTION_ALIGN_Y);
    } else if (key == 48 + 3) {
        doCameraAction(CAMERA_ACTION_ALIGN_X);
    } else if (key == 48 + 9) {
        doCameraAction(CAMERA_ACTION_ALIGN_FLIP);
    } else if (key == 48 + 5) {
        if (mainScreen->cam.getOrtho()) {
            doCameraAction(CAMERA_ACTION_PERSPECTIVE);
        } else {
            doCameraAction(CAMERA_ACTION_ORTHO);
        }
    } else if(key == 48 + 0) {
        doCameraAction(CAMERA_ACTION_DEFAULT);
    } else {
    }
    cout << key << endl;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void Refeynman::pairAlign(const ofVboMesh & mom,
                          const ofVboMesh & child,
                          const RefeynmanParameters & parameters,
                          glm::mat4 & theMatrix){

    ofxPCL::PointCloud momCloud = ofxPCL::toPCL(mom.getVertices());
    ofxPCL::PointCloud childCloud = ofxPCL::toPCL(child.getVertices());
    ofxPCL::ColorPointCloud momCloudColor = ofxPCL::toPCL(mom.getVertices(), mom.getColors());
    ofxPCL::ColorPointCloud childCloudColor = ofxPCL::toPCL(child.getVertices(), child.getColors());

    float score = 0;
    ofxPCL::pairAlignMatrix(momCloud,
                            childCloud,
                            theMatrix,
                            momCloudColor,
                            childCloudColor,
                            "/home/pointer/output_src.pcd",
                            "/home/pointer/output_tgt.pcd",
                            score,
                            parameters.downsample.get(),
                            parameters.leafSize.get(),
                            parameters.transformationEpsilon.get(),
                            parameters.transformRotationEpsilon.get(),
                            parameters.iterations.get(),
                            parameters.maxIterations.get(),
                            parameters.correspondence.get(),
                            parameters.normalSearchRadius.get(),
                            parameters.incrementalCorrespondenceReduction.get());

}

bool Refeynman::saveToDisk(const string & fileName, const glm::mat4 & cameraPoseMatrix){
    cout << "refeynmanMatrix saving to " << fileName << " as " << ofToString(cameraPoseMatrix) << endl;
    double m[16] = {0.0};

    const float * p = (const float *)glm::value_ptr(cameraPoseMatrix);
    for(int i = 0; i < 16; ++i){
        m[i] = p[i];
    }
    ofJson json;
    json["refeynmanMatrix"] = std::vector <float>(m, m + sizeof m / sizeof m[0]);
    bool success = ofSavePrettyJson(fileName, json);
    ofLogNotice("CalibrationwExtrinsics::saveToDisk") << "save extrinsics " << fileName;
    return success;
}

bool Refeynman::loadFromDisk(const string & fileName, glm::mat4 & cameraPoseMatrix){
    ofFile file(fileName);
    if(file.exists()){
        ofJson json = ofLoadJson(fileName);
        vector <float> j = json["refeynmanMatrix"].get <vector <float> >();
        cameraPoseMatrix = glm::make_mat4x4(j.data());
        ofLogNotice("CalibrationwExtrinsics::loadFromDisk") << "loaded refeynmanMatrix " << fileName;
        return true;
    }
    ofLogNotice("CalibrationwExtrinsics::loadFromDisk") << "failed loaded refeynmanMatrix " << fileName;
    return false;
}

