#include "BulletPhysics.h"
#include "type_ptr.hpp"
#ifndef GLM_ENABLE_EXPERIMENTAL
    #define GLM_ENABLE_EXPERIMENTAL
#endif
#include <glm/gtx/dual_quaternion.hpp>
#include <glm/gtc/vec1.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/ext/scalar_int_sized.hpp>
#include <glm/ext/scalar_uint_sized.hpp>
#include <glm/gtx/fast_square_root.hpp>
#include <glm/glm.hpp>


void BulletPhysics::setup(){

    cout << "BULLET VERSION " << ofToString(BT_BULLET_VERSION) << endl;

    collisionConfig = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfig);
    overlappingPairCache = new btDbvtBroadphase();
    solver = new btSequentialImpulseConstraintSolver;
    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfig);
    colShape = new btBoxShape(btVector3(14, 2, 3));
    collisionShapes.push_back(colShape);

    dynamicsWorld->setGravity(btVector3(0.0f, -0.0f, 0.0f));
    //// MARKER

    if(dynamicsWorld->getDebugDrawer()){
        dynamicsWorld->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_DrawWireframe + btIDebugDraw::DBG_DrawContactPoints);
    }
    cout << "SETUP DONE" << endl;
}

void BulletPhysics::update(){
    //lock_guard <mutex> dynamicsWorld_lock(dynamicsWorld_mtx);
    //dynamicsWorld->stepSimulation(parameters.simSpeed.get(), parameters.simSteps.get());

    //for(int i = 0; i < dynamicsWorld->getNonStaticRigidBodies().size(); i++){
    //const auto & bb = dynamicsWorld->getNonStaticRigidBodies().at(i);
    //glm::vec3 pos = glm::vec3(
    //bb->getWorldTransform().getOrigin().getX(),
    //bb->getWorldTransform().getOrigin().getY(),
    //bb->getWorldTransform().getOrigin().getZ()
    //);

    //auto & node = nodes[i];
    //node.setGlobalPosition(pos);
    //node.setOrientation(glm::quat(
    //bb->getOrientation().getW(),
    //bb->getOrientation().getX(),
    //bb->getOrientation().getY(),
    //bb->getOrientation().getZ()
    //));
    //float distance = glm::fastLength(pos);
    //if(distance > 0){
    //float quadratic = distance * distance;
    //float strength = ofRandom(0, 100) / quadratic;
    //glm::vec3 force = strength * (-1 * pos);
    //bb->applyCentralForce(btVector3(force.x, force.y, force.z));
    //}else{
    //float strength = ofRandom(0, 100);
    //glm::vec3 force = strength * (glm::vec3(1, 0, 0));
    //bb->applyCentralForce(btVector3(force.x, force.y, force.z));
    //}
    //}
}

void BulletPhysics::transformBody(int index, const ofNode & node, bool addTransform){
    //glm::mat4 m = node.getLocalTransformMatrix();
    //float * mf = glm::value_ptr(m);
    //btTransform transform;
    //transform.setFromOpenGLMatrix(mf);

    lock_guard <mutex> dynamicsWorld_lock(dynamicsWorld_mtx);
    auto & bb = dynamicsWorld->getNonStaticRigidBodies()[index];
    //bb->setWorldTransform(transform);
    auto originalOrigin = bb->getWorldTransform().getOrigin();
    bb->getWorldTransform().setOrigin(originalOrigin + btVector3(
                                          node.getGlobalPosition().x,
                                          node.getGlobalPosition().y,
                                          node.getGlobalPosition().z));
}

void BulletPhysics::draw(){
}

void BulletPhysics::exit(){
}

void BulletPhysics::createRigidBody(btScalar mass, btTransform transform, btCollisionShape * shape){
    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if(isDynamic){
        shape->calculateLocalInertia(mass, localInertia);
    }

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState * myMotionState = new btDefaultMotionState(transform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,
                                                    myMotionState,
                                                    shape,
                                                    localInertia);
    btRigidBody * body = new btRigidBody(rbInfo);

    //add the body to the dynamics world
    dynamicsWorld->addRigidBody(body);
    cout << "PLANET dynamicsWorld->getCollisionObjectArray().size()" <<  dynamicsWorld->getCollisionObjectArray().size() << endl;
}

void BulletPhysics::addBody(const ofVboMesh & m){
    {
        cout << "Adding body" << endl;
        /// Create Dynamic Objects
        btTransform startTransform;
        startTransform.setIdentity();

        btScalar mass(80.f);

        //rigidbody is dynamic if and only if mass is non zero, otherwise static
        bool isDynamic = (mass != 0.f);

        btVector3 origin = btVector3(0, 0, 0);

        startTransform.setOrigin(origin);

        //btQuaternion rot;
        //rot.setEuler(ofRandom(0, 180), ofRandom(0, 180), ofRandom(0, 180));
        //startTransform.setRotation(rot);

        createRigidBody(mass, startTransform, colShape);
        for(int v = 0; v < colShape->getNumVertices(); v++){
            btVector3 vtx;
            colShape->getVertex(v, vtx);
            //p.debugShape.addVertex(glm::vec3(vtx.getX(), vtx.getY(), vtx.getZ()));
        }
        btCollisionObject * obj = dynamicsWorld->getCollisionObjectArray().at(dynamicsWorld->getCollisionObjectArray().size() - 1);
        btRigidBody * bb = (btRigidBody *)btRigidBody::upcast(obj);

        bb->setActivationState(DISABLE_DEACTIVATION);
        ofNode node;
        node.setGlobalPosition(glm::vec3(
                                   bb->getWorldTransform().getOrigin().getX(),
                                   bb->getWorldTransform().getOrigin().getY(),
                                   bb->getWorldTransform().getOrigin().getZ()
                                   ));
        node.setOrientation(glm::quat(
                                bb->getOrientation().getW(),
                                bb->getOrientation().getX(),
                                bb->getOrientation().getY(),
                                bb->getOrientation().getZ()
                                ));
        nodes.push_back(node);
    }
}

void BulletPhysics::clear(){
    dynamicsWorld->getCollisionObjectArray().clear();
}
